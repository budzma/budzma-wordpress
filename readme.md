# README #
This describes how to get budzma.by up and running locally.

### How to set up local website? ###
#### 1) Clone this repo :) ####
#### 2) Get content images and files (if you need them) ####
Content images and files (PDF, MP3, SWF etc.) are NOT included in the repo, so if you want to see them locally, copy them from the live server (there is more than 5GB overall). This is the list of excluded content folders:

- `/wp-content/upload`
- `/cards`
- `/banery`
- `/swf`

#### 3) Set up the database ####
Get the latest database dump from the live server. It's pretty big: about 500 MB uncompressed `.sql` and about 70 MB compressed in `.gz`. 

Since creating a database via web tools (like phpMyAdmin) may take ages and hit all your php environment limits, it is recommended to create a database via command line with `mysql` tool:

- run your command line
- go to mysql path of your server, something like `...\mysql5.6.12\bin`
- then connect to your root:  `mysql -u root -p -h 127.0.0.1`
- enter password if you have one
- create a new database: `create database budzmaorg;`
- use it: `use budzmaorg;`
- and finally import the file (enter full path to the unpacked SQL file): `source c:/budzmaorg.sql;`

Change database user and password in `/wp-content.php`

#### 4) Set up local site URL ####
Change site url to your local name (like budzma.dev or whatever you use) in the following options in the table **wp_options**:

- siteurl
- home

#### 5) Enjoy :) ####