﻿<?php
/**
* Organizer metabox
*/

// Don't load directly
if ( !defined('ABSPATH') ) { die('-1'); }

?>
<?php if ( empty($hide_organizer_title) ): ?>

<?php endif; ?>
<tr class="organizer">
	<td><?php _e('Тэлефон:','tribe-events-calendar'); ?></td>
	<td><input tabindex="<?php $this->tabIndex(); ?>" type='text' id='OrganizerPhone' name='organizer[Phone]' size='25' value='<?php echo isset($_OrganizerPhone) ? esc_attr($_OrganizerPhone) : ""; ?>' /></td>
</tr>

<tr class="organizer">
	<td><?php _e('Email:','tribe-events-calendar'); ?></td>
	<td><input tabindex="<?php $this->tabIndex(); ?>" type='text' id='OrganizerEmail' name='organizer[Email]' size='25' value='<?php echo isset($_OrganizerEmail) ? esc_attr($_OrganizerEmail) : ""; ?>' /></td>
</tr>

<script type="text/javascript">
	jQuery('[name=organizer\\[Organizer\\]]').blur(function(){
		jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>',
			{
				action: 'tribe_event_validation',
				nonce: '<?php echo wp_create_nonce('tribe-validation-nonce'); ?>',
				type: 'organizer',
				name: jQuery('[name=organizer\\[Organizer\\]]').get(0).value
			},
			function(result) {
				if (result == 1) {
					jQuery('[name=organizer\\[Organizer\\]]').parent().removeClass('invalid').addClass('valid');
				} else {
					jQuery('[name=organizer\\[Organizer\\]]').parent().removeClass('valid').addClass('invalid');
				}
			}
		);
	});
</script>