﻿
<link rel="stylesheet" type="text/css" href="<? bloginfo('url'); ?>/wp-content/plugins/the-events-calendar/views/style-a.css"/>


<?php
/**
 * Events List Widget Template
 * This is the template for the output of the events list widget. 
 * All the items are turned on and off through the widget admin.
 * There is currently no default styling, which is needed.
 *
 * This view contains the filters required to create an effective events list widget view.
 *
 * You can recreate an ENTIRELY new events list widget view by doing a template override,
 * and placing a list-widget.php file in a tribe-events/widgets/ directory 
 * within your theme directory, which will override the /views/widgets/list-widget.php.
 *
 * You can use any or all filters included in this file or create your own filters in 
 * your functions.php. In order to modify or extend a single filter, please see our
 * readme on templates hooks and filters (TO-DO)
 *
 * @return string
 *
 * @package TribeEventsCalendar
 * @since  2.1
 * @author Modern Tribe Inc.
 *
 */
?>

<li class="tribe-events-list-widget-events">
<div class="event-budzma"> 
        <?
        if( tribe_show_google_map_link( get_the_ID() ) )
        { echo ' <a href="#" title="Падзея ў межах кампаніі Будзьма Беларусамі!"><img src="http://new.budzma.org/wp-content/themes/BUDZMAorg/images/p1.png" alt="!" width="13" height="13"/></a> '; } ?>
        </div>	
		<?php echo tribe_get_start_date( null, false, 'd.m' ); ?> - <a href="<?php echo tribe_get_event_link(); ?>" rel="bookmark"><?php the_title(); ?></a>
		
	<!-- Event Time -->
	
</li>
