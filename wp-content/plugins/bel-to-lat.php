<?php
/*
Plugin Name: BelToLat
Plugin URI: http://aliaksei.org/plugins/beltolat.html
Description: Транслітэрацыя кірылічных сімвалаў у лацінку
Author: Aliaksei <aliaksei.by@gmail.com>
Author URI: http://www.aliaksei.org/
Version: 0.2.6
*/ 

$translit = array(
   "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
   "Е"=>"E","Ё"=>"JO","Ж"=>"ZH","З"=>"Z",
   "И"=>"I","І"=>"I","Й"=>"JJ","К"=>"K","Л"=>"L",
   "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
   "С"=>"S","Т"=>"T","У"=>"U","Ў"=>"U","Ф"=>"F","Х"=>"KH",
   "Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
   "Ы"=>"Y","Ь"=>"","Э"=>"EH","Ю"=>"YU","Я"=>"YA",
   "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
   "е"=>"e","ё"=>"jo","ж"=>"zh","з"=>"z",
   "и"=>"i","і"=>"i","й"=>"jj","к"=>"k","л"=>"l",
   "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
   "с"=>"s","т"=>"t","у"=>"u","ў"=>"u","ф"=>"f","х"=>"kh",
   "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
   "ы"=>"y","ь"=>"","э"=>"eh","ю"=>"yu","я"=>"ya",
   "Є"=>"EH","є"=>"eh","Ґ"=>"G","ґ"=>"g",

   "№"=>"#","«"=>"","»"=>"","—"=>"-", "“"=>"", "”"=>""
  );

$bgn = array(
   "А"=>"A","Б"=>"B","В"=>"V","Г"=>"H","Д"=>"D",
   "Е"=>"YE","Ё"=>"YO","Ж"=>"ZH","З"=>"Z",
   "І"=>"I","Й"=>"Y","К"=>"K","Л"=>"L",
   "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
   "С"=>"S","Т"=>"T","У"=>"U","Ў"=>"W","Ф"=>"F","Х"=>"KH",
   "Ц"=>"TS","Ч"=>"CH","Ш"=>"SH","Ы"=>"Y","Ь"=>"'",
   "Э"=>"E","Ю"=>"YU","Я"=>"YA",
   
   "а"=>"a","б"=>"b","в"=>"v","г"=>"h","д"=>"d",
   "е"=>"ye","ё"=>"yo","ж"=>"zh","з"=>"z",
   "і"=>"i","й"=>"y","к"=>"k","л"=>"l",
   "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
   "с"=>"s","т"=>"t","у"=>"u","ў"=>"w","ф"=>"f","х"=>"kh",
   "ц"=>"c","ч"=>"ch","ш"=>"sh","ы"=>"y","ь"=>"'",
   "э"=>"e","ю"=>"yu","я"=>"ya",

   "Ґ"=>"G","ґ"=>"g",
   
   "И"=>"I","Щ"=>"SCH","Ъ"=>"'","ъ"=>"","Э"=>"EH",
   "и"=>"i","щ"=>"sch","є"=>"eh",
   "№"=>"#","«"=>"","»"=>"","—"=>"-", "“"=>"", "”"=>""
  );


function sanitize_title_with_translit($title) {
	global $translit, $bgn;
	switch (get_option('btl_standart')) {
		case 'translit':
		    return strtr($title, $translit);
		case 'bgn':
		    return strtr($title, $bgn);
		case 'off':
		    return $title;		
		default: 
		    return strtr($title, $bgn);		
	}
}

function btl_options_page() {
?>
<div class="wrap">
	<h2>Налады BelToLat</h2>
	<br/>
	<form method="post" action="options.php">
	<?php wp_nonce_field('update-options'); ?>
	<fieldset class="options">
		<?php $btl_standart = get_option('btl_standart'); ?>
		<label>У якім фармаце рабіць трансліт:</label>
		<select name="btl_standart">
			<option value="off" <?php if($btl_standart == 'off'){ echo('selected="selected"');}?>>Выключана</option>
			<option value="bgn" <?php if($btl_standart == 'bgn' OR $btl_standart == ''){ echo('selected="selected"');}?>>BGN/PCGN</option>
			<option value="translit" <?php if($btl_standart == 'translit'){ echo('selected="selected"');}?>>Трансліт</option>
		</select>
	</fieldset>
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="page_options" value="btl_standart" />
	<input type="submit" value="Змяніць" />
	</form>
	<p><a href="http://be.wikipedia.org/wiki/BGN/PCGN_раманізацыя_беларускай_мовы">BGN/PCGN раманізацыя беларускай мовы</a> — сістэма раманізацыі (запісу лацінскім пісьмом) беларускага кірылічнага тэксту, распрацаваная супольна ў Радзе па геаграфічных назвах ЗША (BGN, United States Board on Geographic Names) і ў Пастаянным камітэце па геаграфічных назвах для брытанскага афіцыйнага выкарыстання (PCGN, Permanent Committee on Geographical Names for British Official Use).</p>
	<p>Трансліт - звычайны інтэрнэт трансліт</p>
	<p><small>Незалежна ад стандарта, дзеля зручнасці карыстання, таксама робіцца трансліт літар "И, Щ, Ъ" і некаторых знакаў</small><p>
</div>
<?php
}

function btl_add_menu() {
		add_options_page('BelToLat', 'BelToLat', 8, __FILE__, 'btl_options_page');
}

add_action('admin_menu', 'btl_add_menu');
add_action('sanitize_title', 'sanitize_title_with_translit', 0);
?>