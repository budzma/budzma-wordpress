<? get_header(); ?>
 
 
<div class="block"> <!-- +sidebar -->

<div class="single-post"> <!-- Пост граніца 10px -->

<?php if (have_posts()) : ?>

				<header class="page-header">
					<h2>
						<?php if (is_category()) { ?>
							Раздзел: <?php single_cat_title(); ?>
						<?php } elseif( is_tag() ) { ?>
							Тэма: <?php single_tag_title(); ?>
						<?php } elseif (is_day()) { ?>
							Архіў за <?php the_time('d.m.Y'); ?>
						<?php } elseif (is_month()) { ?>
							Архіў за <?php the_time('F, Y'); ?>
						<?php } elseif (is_year()) { ?>
							Архіў за <?php the_time('Y'); ?> год
						<?php } elseif (is_author()) { ?>
							<?php $curauth = get_userdata(intval($author)); ?>
							Аўтар: <?php echo $curauth->display_name; ?>
						<?php } ?>
					</h2>
				</header>

 
 <? while (have_posts()): the_post(); ?>
  <div class="excerp-post" > <!-- start post -->
    <hr noshade size="1"/>
    <h2 class="entry-title-excerpt"><div class="sam_title"><a href="<? the_permalink(); ?>" rel="bookmark"> <? the_title(); ?></a></div>
    
    
    
    <?php if ( in_category('photoreport') & in_category('video') ) : ?> <!-- определение принадлежности поста к категории -->
        <div class="after_title_photo"><a href="<? the_permalink(); ?>" rel="bookmark" title="Фотарэпартаж"><img src="<? bloginfo('template_url');?>/img/photo.png"/></a></div>
        <div class="after_title"><a href="<? the_permalink(); ?>" rel="bookmark" title="Відэа"><img src="<? bloginfo('template_url');?>/img/video.png"/></a></div>
        <?php elseif ( in_category('video') ) : ?>
            <div class="after_title"><a href="<? the_permalink(); ?>" rel="bookmark" title="Відэа"><img src="<? bloginfo('template_url');?>/img/video.png"/></a></div> 
        <?php elseif ( in_category('photoreport') ) : ?> <!-- определение принадлежности поста к категории -->
        <div class="after_title"><a href="<? the_permalink(); ?>" rel="bookmark" title="Фотарэпартаж"><img src="<? bloginfo('template_url');?>/img/photo.png"/></a></div>    
    <?php else : ?> 
         <div class="after_title">&nbsp;</div> 
    <?php endif; ?>
   
    
    </h2>
    
     
   
    
   
    
    <div class="entry-meta">
				<!--	<p><time pubdate datetime="2012-08-17T15:59:33+00:00">17 жніўня 2012 15:59</time> | <a href="http://budzma.org/category/budzma" title="Прагледзець усе запісы ў рубрыцы &laquo;Навіны &quot;Будзьма!&quot;&raquo;" rel="category tag">Навіны "Будзьма!"</a>   | <a class="post-edit-link" href="http://budzma.org/wp-admin/post.php?post=60166&amp;action=edit" title="Рэдагаваць запіс">Рэдагаваць</a> </p> -->
            <p><time pubdate datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('j F Y H:i'); ?></time> | <?php the_category(', '); ?> <?php edit_post_link(NULL, ' | '); ?> </p>
    </div>
    
    
    <div class="soderzhanie2">
    
   <a href="<? the_permalink(); ?>" > <? the_post_thumbnail(); ?> </a>
    
        <div class="soderzhanie2-text">
        
    <? the_excerpt(); ?>  <!--  <? the_content(); ?> --> <!-- <a href="<? the_permalink(); ?>">Далей »</a> -->
        
        </div>
    </div>
  </div>    <!-- .excerp-post -->   <!-- start post -->
  <? endwhile; ?>
  <hr noshade size="1"/> 
  
    <div class="navigator-3w" align="center"> <!-- навигатор -->
  
     <?php
				if (function_exists('wp_pagenavi')) {
					wp_pagenavi();
				} else { ?>
					<div class="alignleft"><?php next_post_link('&larr; папярэднія') ?></div>
					<div class="alignright"><?php previous_post_link('наступныя &rarr;') ?></div>
				<?php } ?>
                
                <div class="rss">
                <!-- URL текущей страницы: <?php bloginfo('url'); echo $_SERVER["REQUEST_URI"] ?>
                ++ Url текущей категории: <?php echo get_category_link($cat);?>                
                 -->                
                <a href="<?php echo get_category_link($cat);?>/feed"><img src="<? bloginfo('template_url');?>/img/rss.jpg" width="18" height="18"/></a>&nbsp;<a href="<?php echo get_category_link($cat);?>/feed">RSS «<?php single_cat_title(); ?>»</a>  
 

 <!-- Гісторыя RSS-стужка ўсіх запісаў у рубрыцы «Гісторыя» (569) 
 <ul>
<?php wp_list_categories('orderby=name&show_count=1&feed_image=/images/rss.gif'); ?>
</ul> 
  -->              
                
                </div>
    </div>                
 <? endif; ?>  
</div>


<? get_sidebar(); ?> <!-- подключаем sidebar -->
 
 
</div>  <!-- end block -->



<? get_footer(); ?>