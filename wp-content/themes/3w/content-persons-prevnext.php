<?php
  /**
   * Файл content-persons-prevnext.php
   * 
   * Превью следующей и предыдущей асоб.
   * Вызывается через get_template_part после вызова процедуры bpers_set_tpl_vars.
   * Для получения переменных исспользуется global $post, $post->bpers['prev_post'] и $post->bpers['next_post'].   
   * 
   * Для CSS:
   * #port-bottom
   * .portrait-white-border
   * .portrait-image
   * .portrait-name
   * .portrait-name-text
   * .red-footer-slogan                  
   */     
?>

<?php global $post; ?> 
<!-- последний ряд портретов-->
  <div id="port-bottom">
    
    <!-- портрет с белой рамкой-->    
    <div class="portrait-white-border">
      <div class="portrait-image">
        <img width="<?php echo $post->bpers['prev_post']['thumb']['width']; ?>" height="<?php echo $post->bpers['prev_post']['thumb']['height']; ?>" src="<?php echo $post->bpers['prev_post']['thumb']['url']; ?>" alt="<?php echo $post->bpers['prev_post']['title']; ?>" />
        <div class="portrait-name">
          <div class="portrait-name-text">
            <a href="<?php echo $post->bpers['prev_post']['permalink']; ?>">
              <?php echo $post->bpers['prev_post']['title']; ?>
              <br />
              <span>
                <?php echo $post->bpers['prev_post']['slogan']; ?>
              </span>
            </a>
          </div>
        </div>
      </div>
    </div>
    
    <!-- красный логотип-->
    <div class="red-footer-slogan">
    </div>
    
    <!-- портрет с белой рамкой-->
    <div class="portrait-white-border">
      <div class="portrait-image">
        <img width="<?php echo $post->bpers['next_post']['thumb']['width']; ?>" height="<?php echo $post->bpers['next_post']['thumb']['height']; ?>" src="<?php echo $post->bpers['next_post']['thumb']['url']; ?>" alt="<?php echo $post->bpers['next_post']['title']; ?>" /> 
        <div class="portrait-name">
          <div class="portrait-name-text">
            <a href="<?php echo $post->bpers['next_post']['permalink']; ?>">
              <?php echo $post->bpers['next_post']['title']; ?>
              <br />
              <span>
                <?php echo $post->bpers['next_post']['slogan']; ?>
              </span>
            </a>
          </div>
        </div>
      </div>
    </div>
    
  </div>