<?php

/**
 * Файл content-persons-cat.php.
 * Шаблон отображает список тизеров для стр. категории или главной стр. 
 * 
 * Для CSS:
 * .portrait-white-border                     
 * .portrait-red-border
 * .portrait-image
 * .portrait-name
 * .portrait-name-text
 * .grey-footer-slogan
 * #text-more   
 */

// число постов на страницу на 1 меньше чем мест (одно для техн. нужд)
$post_per_page = 14;

// подсчитываю кол-во постов которые нужно отобразить 
global $wp_query;
$post_for_page = count($wp_query->posts);
//wp_pear_debug::dump($wp_query->posts, '1');

if ($post_for_page < $post_per_page) {
  // число полностью заполненых линий (полос по три поста)
  $full_lines = intval(floor($post_for_page/3));
  //wp_pear_debug::dump($full_lines, '$full_lines');
  
  // число МЕСТ под посты (!! из-за особенностей диза оно может отличаться от числа постов: 
  // последняя линия должна быть построена вся, даже если постов больше нет)
  $post_per_page = ($full_lines + 1)*3 - 1;
  //wp_pear_debug::dump($post_per_page, 'post_per_page');
}

$i = 0;    
if (have_posts()) {
  // можно строить страницу 
  while (have_posts()) {
    the_post();
    $i++; 
    // для четных постов белая рамка
    $css_class = 'portrait-white-border';
    // для нечетных постов красная рамка
    if ($i%2) {                     
      $css_class = 'portrait-red-border';
    }
    // перед последним постом 
    if ($i == $post_per_page) { 
      // всегда серый
      bpers_cat_prelast_item();
      // возвращаю на красный 
      $css_class = 'portrait-red-border'; 
    }   
    bpers_cat_item( $css_class);
  }

  // посты кончились, а места остались
  if ($i < $post_per_page) {
    while ($i < $post_per_page) {
      $i++;
      $css_class = 'portrait-white-border';
      //if ($i%2) {
      //  $css_class = 'portrait-red-border';
      //}
      // перед последним постом 
      if ($i == $post_per_page) { 
        bpers_cat_prelast_item();
        //$css_class = 'portrait-red-border'; 
      } 
      bpers_cat_stub_item( $css_class);
		 }
   }
}  

?>    
    
<?php
/**
 * Дизайн и верста такие хитрые, что повторяющуюся часть разметки с условным css
 * пришлось обернуть в функции. 
 */ 

function bpers_cat_item( $css_class) { ?>
  <div class="<?php print $css_class; ?>">
    <div class="portrait-image">
      <?php the_post_thumbnail('full'); ?>
      <div class="portrait-name">
        <div class="portrait-name-text">
          <a href="<?php echo get_permalink(); ?>">
            <?php the_title(); ?><br /><span><?php the_field('slogan'); ?></span>
          </a>
        </div>
      </div>
    </div>
  </div> <?php 
}

function bpers_cat_prelast_item() { ?>
  <div class="grey-footer-slogan">
    <div class="text-more">
      <?php previous_posts_link('&larr; папярэднія'); ?>
      <?php next_posts_link('наступныя &rarr;') ?>
    </div>
  </div>
<?php
}

function _bpers_cat_stub_item() { ?>
  <div class="<?php print $css_class; ?>">
    <div class="portrait-image">
      <img src="<?php echo get_template_directory_uri(); ?>/persons/img/stub_portrait.gif" />
      <div class="portrait-name">
      </div>
    </div>
	</div>
<?php
}

function bpers_cat_stub_item( $css_class) { ?>
  <div class="<?php print $css_class; ?>">
    <div class="portrait-image">
      <img width="250" height="250" class="attachment-full wp-post-image" src="<?php echo get_template_directory_uri(); ?>/persons/img/stub_portrait.gif" />
    </div>
  </div> <?php
}

function bpers_tmp( $css_class) { ?>
  <div class="<?php print $css_class; ?>">
    <div class="portrait-image">
      <div class="grey-footer-slogan">
        <div class="text-more">
          cfbgjkgfj
        </div>
      </div>
    </div>
  </div>
<?php
}

?>