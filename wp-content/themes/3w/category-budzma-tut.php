<?php
/**
 * @package WordPress
 * @subpackage Budzma_2011
 */
get_header(); ?>

<div id="content" role="main">

	<div class="posts-wrapper">

		<section class="archive">

			<?php if (have_posts()) : ?>

				<header class="page-header">
					<h2><span><!----></span>Блог</h2>
				</header>

				<?php while (have_posts()) : the_post(); ?>

					<?php get_template_part('content', get_post_format()); ?>

				<?php endwhile; ?>

				<?php
				if (function_exists('wp_pagenavi')) {
					wp_pagenavi();
				} else { ?>
					<div class="alignleft"><?php next_posts_link('&larr; папярэднія') ?></div>
					<div class="alignright"><?php previous_posts_link('наступныя &rarr;') ?></div>
				<?php } ?>

			<?php else : ?>

				<?php get_template_part('error'); ?>

			<?php endif; ?>
		</section>

	</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

<?php //get_template_part('footer-budzma-tut'); ?>