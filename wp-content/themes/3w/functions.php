<?php

if(function_exists('register_sidebar'))
register_sidebar(array('name'=>'sidebar')); //регистрируем сайдбар (блок виджетов)
register_sidebar(array('name'=>'sidebar2-afisha')); //регистрируем сайдбар (блок виджетов)
register_sidebar(array('name'=>'sidebar3')); //регистрируем сайдбар ( дополнительный блок виджетов)

register_sidebar(array('name'=>'footer')); // регистрируем футер комментариев
register_sidebar(array('name'=>'footer-most-viewed')); // регистрируем футер комментариев

register_sidebar(array('name'=>'index-tab')); // регистрируем tab

    add_theme_support('post-thumbnails'); // поддержка миниатюр
    set_post_thumbnail_size(150, 150, TRUE); // УКАЗЫВАЕМ РАЗМЕР миниатюры. TRUE - обрезка по центру, "FALSE" - пропорционально (по умолчанию)






	function register_my_menus()
	{
	register_nav_menus
	(
	array( 'header-menu' => 'header-menu1', 'header-menu-sub' => 'header-menu1-sub', 'header-menu2' => 'menu2', 'footer-menu' => 'footer-menu1')
	);
	} /* header-menu2 - верхнее главное меню; включение в теме:  <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?> */

	if (function_exists('register_nav_menus'))
	{
	     add_action( 'init', 'register_my_menus' );
	}

/* ограничение числа слов: return 30  */
function new_excerpt_length($length) {
    return 38;
}
add_filter('excerpt_length', 'new_excerpt_length');

function new_excerpt_more($more) {
       global $post;
    return '<a href="'. get_permalink($post->ID) . '">' . '&nbsp;(далей…)' . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

/**
 * Раздел "Асобы".
 */


function bpers_set_tpl_vars() {
  bpers_set_tpl_vars_post();
  bpers_set_tpl_vars_prev_post();
  bpers_set_tpl_vars_next_post();
  global $post;
  //wp_pear_debug::dump( $post);
}

function bpers_set_tpl_vars_post() {
  global $post;
  //wp_pear_debug::dump( $post);
  if ($post->post_type == 'asoba') {
    $first = array();
    $first['type'] = FALSE; // говорит о том, что профайл сломан
    $second = array();
    $second['type'] = FALSE; // говорит о том, что профайл сломан

    $custom = get_post_custom();
    //wp_pear_debug::dump( $custom);

    $permalink = get_permalink( $post->ID);

    $title = $post->post_title;

    $title_few_lines = '';
    if (isset($custom['title_few_lines'][0])) {
      $title_few_lines = $custom['title_few_lines'][0];
    }
    //wp_pear_debug::dump( $title_few_lines);

    $slogan = '';
    if (isset($custom['slogan'][0])) {
      $slogan = $custom['slogan'][0];
    }

    $slogan_few_lines = '';
    if (isset($custom['slogan_few_lines'][0])) {
      $slogan_few_lines = $custom['slogan_few_lines'][0];
    }

    $text_first_column = '';
    if (isset($custom['column1'][0])) {
      $text_first_column = $custom['column1'][0];
    }

    $text_second_column = '';
    if (isset($custom['column2'][0])) {
      $text_second_column = $custom['column2'][0];
    }

    $photo_first = FALSE;
    if (isset($custom['photo'][0])) {
      $tmp = wp_get_attachment_image_src( $custom['photo'][0], 'full');
      if ($tmp) {
        $photo_first = array(
          'url' => $tmp[0],
          'width' => $tmp[1],
          'height' => $tmp[2],
        );

        if ($photo_first['width'] < $photo_first['height']) {
          $first['type'] = 'vertical';
        }
        else {
          $first['type'] = 'horizontal';
        }
      }
    }
    //wp_pear_debug::dump( $photo_first);

    $photo_second = '';
    if (isset($custom['photo2'][0])) {
      $tmp = wp_get_attachment_image_src( $custom['photo2'][0], 'full');
      if ($tmp) {
        $photo_second = array(
          'url' => $tmp[0],
          'width' => $tmp[1],
          'height' => $tmp[2],
        );

        if ($photo_second['width'] < $photo_second['height']) {
          $second['type'] = 'vertical';
        }
        else {
          $second['type'] = 'horizontal';
        }
      }
    }
    //wp_pear_debug::dump( $photo_second);

    $thumb_first = FALSE;
    if (isset($custom['_thumbnail_id'][0])) {
      $tmp = wp_get_attachment_image_src( $custom['_thumbnail_id'][0], 'full');
      if ($tmp) {
        $thumb_first = array(
          'url' => $tmp[0],
          'width' => $tmp[1],
          'height' => $tmp[2],
        );
      }
    }
    //wp_pear_debug::dump( $thumb_first);

    $thumb_second = FALSE;
    if (isset($custom['thumb2'][0])) {
      $tmp = wp_get_attachment_image_src( $custom['thumb2'][0], 'full');
      if ($tmp) {
        $thumb_second = array(
          'url' => $tmp[0],
          'width' => $tmp[1],
          'height' => $tmp[2],
        );
      }
    }
    //wp_pear_debug::dump( $thumb_second);

    //wp_pear_debug::dump( $first);
    //wp_pear_debug::dump( $second);

    $terms = array();
    $term = FALSE;
    $tmp = get_the_terms( $post->ID, 'persons');
    if (is_array($tmp) && !empty($tmp)) {
      $terms = $tmp;
      $term = array_shift( $tmp);
    }
    //wp_pear_debug::dump( $term);

    // можно будет сменить формат

    $post->bpers = array();

    $post->bpers['permalink'] = $permalink;
    $post->bpers['term'] = $term;

    $post->bpers['title'] = $title;
    $post->bpers['title_few_lines'] = $title_few_lines;

    $post->bpers['slogan'] = $slogan;
    $post->bpers['slogan_few_lines'] = $slogan_few_lines;

    $post->bpers['thumb'] = $thumb_first;

    $post->bpers['text_first_column'] = $text_first_column;
    $post->bpers['text_second_column'] = $text_second_column;

    $post->bpers['first'] = $first;
    $post->bpers['first']['photo'] = $photo_first;
    $post->bpers['first']['thumb'] = $thumb_first;

    $post->bpers['second'] = $second;
    $post->bpers['second']['photo'] = $photo_second;
    $post->bpers['second']['thumb'] = $thumb_second;

    //wp_pear_debug::dump( $post->bpers);
  }
}

function bpers_set_tpl_vars_prev_post() {
  global $post;
  if (isset($post->bpers)) {
    $prev_post = array_shift( get_adjacent_post_plus(
      array(
        'order_by' => 'post_date',
        'loop' => true,
        'end_post' => false,
        'in_same_cat' => 'persons',
        'in_same_tax' => 'persons',
        'echo' => false,
      ),
      true
    ));

    if (isset($prev_post->ID)) {
      $custom = get_post_custom( $prev_post->ID);
      //wp_pear_debug::dump( $custom);

      $permalink = get_permalink( $prev_post->ID);

      $title = $prev_post->post_title;

      $title_few_lines = '';
      if (isset($custom['title_few_lines'][0])) {
        $title_few_lines = $custom['title_few_lines'][0];
      }

      $slogan = '';
      if (isset($custom['slogan'][0])) {
        $slogan = $custom['slogan'][0];
      }

      $slogan_few_lines = '';
      if (isset($custom['slogan_few_lines'][0])) {
        $slogan_few_lines = $custom['slogan_few_lines'][0];
      }

      $thumb_first = FALSE;
      if (isset($custom['_thumbnail_id'][0])) {
        $tmp = wp_get_attachment_image_src( $custom['_thumbnail_id'][0], 'full');
        if ($tmp) {
          $thumb_first = array(
            'url' => $tmp[0],
            'width' => $tmp[1],
            'height' => $tmp[2],
          );
        }
      }
      //wp_pear_debug::dump( $thumb_first);

      $thumb_second = FALSE;
      if (isset($custom['thumb2'][0])) {
        $tmp = wp_get_attachment_image_src( $custom['thumb2'][0], 'full');
        if ($tmp) {
          $thumb_second = array(
            'url' => $tmp[0],
            'width' => $tmp[1],
            'height' => $tmp[2],
          );
        }
      }
      //wp_pear_debug::dump( $thumb_second);

      $terms = array();
      $term = FALSE;
      $tmp = get_the_terms( $prev_post->ID, 'persons');
      if (is_array($tmp) && !empty($tmp)) {
        $terms = $tmp;
        $term = array_shift( $tmp);
      }
      //wp_pear_debug::dump( $term);

      // можно будет сменить формат

      $post->bpers['prev_post'] = array();

      $post->bpers['prev_post']['permalink'] = $permalink;
      $post->bpers['prev_post']['term'] = $term;

      $post->bpers['prev_post']['title'] = $title;
      $post->bpers['prev_post']['title_few_lines'] = $title_few_lines;

      $post->bpers['prev_post']['slogan'] = $slogan;
      $post->bpers['prev_post']['slogan_few_lines'] = $slogan_few_lines;

      $post->bpers['prev_post']['thumb'] = $thumb_first;

      //wp_pear_debug::dump( $post->bpers['prev_post']);
    }
  }
}

function bpers_set_tpl_vars_next_post() {
  global $post;
  if (isset($post->bpers)) {
    $next_post = array_shift( get_adjacent_post_plus(
      array(
        'order_by' => 'post_date',
        'loop' => true,
        'end_post' => false,
        'in_same_cat' => 'persons',
        'in_same_tax' => 'persons',
        'echo' => false,
      ),
      false
    ));

    if (isset($next_post->ID)) {
      $custom = get_post_custom( $next_post->ID);
      //wp_pear_debug::dump( $custom);

      $permalink = get_permalink( $next_post->ID);

      $title = $next_post->post_title;

      $title_few_lines = '';
      if (isset($custom['title_few_lines'][0])) {
        $title_few_lines = $custom['title_few_lines'][0];
      }

      $slogan = '';
      if (isset($custom['slogan'][0])) {
        $slogan = $custom['slogan'][0];
      }

      $slogan_few_lines = '';
      if (isset($custom['slogan_few_lines'][0])) {
        $slogan_few_lines = $custom['slogan_few_lines'][0];
      }

      $thumb_first = FALSE;
      if (isset($custom['_thumbnail_id'][0])) {
        $tmp = wp_get_attachment_image_src( $custom['_thumbnail_id'][0], 'full');
        if ($tmp) {
          $thumb_first = array(
            'url' => $tmp[0],
            'width' => $tmp[1],
            'height' => $tmp[2],
          );
        }
      }
      //wp_pear_debug::dump( $thumb_first);

      $thumb_second = FALSE;
      if (isset($custom['thumb2'][0])) {
        $tmp = wp_get_attachment_image_src( $custom['thumb2'][0], 'full');
        if ($tmp) {
          $thumb_second = array(
            'url' => $tmp[0],
            'width' => $tmp[1],
            'height' => $tmp[2],
          );
        }
      }
      //wp_pear_debug::dump( $thumb_second);

      $terms = array();
      $term = FALSE;
      $tmp = get_the_terms( $next_post->ID, 'persons');
      if (is_array($tmp) && !empty($tmp)) {
        $terms = $tmp;
        $term = array_shift( $tmp);
      }
      //wp_pear_debug::dump( $term);

      // можно будет сменить формат

      $post->bpers['next_post'] = array();

      $post->bpers['next_post']['permalink'] = $permalink;
      $post->bpers['next_post']['term'] = $term;

      $post->bpers['next_post']['title'] = $title;
      $post->bpers['next_post']['title_few_lines'] = $title_few_lines;

      $post->bpers['next_post']['slogan'] = $slogan;
      $post->bpers['next_post']['slogan_few_lines'] = $slogan_few_lines;

      $post->bpers['next_post']['thumb'] = $thumb_first;

      //wp_pear_debug::dump( $post->bpers['next_post']);
    }
  }
}

function bpers_set_first_current() {
  global $post;
  if ($post->post_type == 'asoba' && isset($post->bpers['first']) && $post->bpers['first']['type']) {
    $post->bpers['current'] = $post->bpers['first'];
    $post->bpers['current']['css_id'] = 'first';
  }
}
function bpers_set_second_current() {
  global $post;
  if ($post->post_type == 'asoba' && isset($post->bpers['second']) && $post->bpers['second']['type']) {
    $post->bpers['current'] = $post->bpers['second'];
    $post->bpers['current']['css_id'] = 'second';
  }
}

function featuredtoRSS($content) {
global $post;
if ( has_post_thumbnail( $post->ID ) ){
$content = '' . get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'style' => 'float:left; margin:0 15px 15px 0;' ) ) . '' . $content;
}
return $content;
}
add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');
?>