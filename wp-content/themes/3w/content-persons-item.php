<?php 
  /**
   * Первый профайл, тот что активен изначально.
   */ 
  global $post; 
  wp_pear_debug::dump( $post->bpers);
?>

<div id="content-<?php echo $post->bpers['current']['css_id']; ?>"<?php if ($post->bpers['current']['css_id'] == 'second') echo ' class="hidden-content"'; ?>>
  <?php if ($post->bpers['current']['type'] == 'vertical') : ?>
    <div class="left">
      <div class="bp-item-v-profile-name">
        <h2>
          <?php echo $post->bpers['title_few_lines']; ?>
        </h2>
      </div>
      <div class="bp-item-v-profile-content">
        <h3>
          <?php echo $post->bpers['slogan_few_lines']; ?>
        </h3>
        <?php echo $post->bpers['text_first_column']; ?>
        <?php echo $post->bpers['text_second_column']; ?>
      </div>
      <!-- портрет с красной подложкой-->
      <div class="bp-item-v-portait-with-red-bg">
        <div class="portraitImage" id="switcher-<?php echo $post->bpers['current']['css_id']; ?>">
          
            <img width="<?php echo $post->bpers['current']['thumb']['width']; ?>" height="<?php echo $post->bpers['current']['thumb']['height']; ?>" src="<?php echo $post->bpers['current']['thumb']['url']; ?>" alt="<?php echo $post->bpers['slogan']; ?>" />
         
        </div>
      </div>
    </div>
    <div class="bp-item-v-center-cont">
      <img width="<?php echo $post->bpers['current']['photo']['width']; ?>" height="<?php echo $post->bpers['current']['photo']['height']; ?>" src="<?php echo $post->bpers['current']['photo']['url']; ?>" alt="<?php echo $post->bpers['title']; ?>" /> 
    </div>
  <?php elseif ($post->bpers['current']['type'] == 'horizontal') : ?>	 
    <div class="right">
      <!-- портрет с красной подложкой-->
      <div class="bp-item-h-portait-with-red-bg-hor">
        <div class="bp-item-h-portrait-image" id="switcher-<?php echo $post->bpers['current']['css_id']; ?>">
          
            <img width="<?php echo $post->bpers['current']['thumb']['width']; ?>" height="<?php echo $post->bpers['current']['thumb']['height']; ?>" src="<?php echo $post->bpers['current']['thumb']['url']; ?>" alt="<?php echo $post->bpers['slogan']; ?>" />
          
        </div>
      </div>
    </div>
    <div class="left">
      <div class="bp-item-h-profile-name">
        <h2>
          <?php echo $post->bpers['title_few_lines']; ?>
        </h2>
      </div>
      <div class="bp-item-h-profile-content">
        <h3>
          <?php echo $post->bpers['slogan_few_lines']; ?>
        </h3>
        <?php echo $post->bpers['text_first_column']; ?>
      </div>
    </div>
    <div class="center">
      <div class="bp-item-h-profile-content">
        <?php echo $post->bpers['text_second_column']; ?>
      </div>
    </div>
    <!--большая фотография-->
    <div class="bp-item-h-profile-big-image-horiz">
      <img width="<?php echo $post->bpers['current']['photo']['width']; ?>" height="<?php echo $post->bpers['current']['photo']['height']; ?>" src="<?php echo $post->bpers['current']['photo']['url']; ?>" alt="<?php echo $post->bpers['title']; ?>" /> 
    </div> 
	<?php endif; ?>
</div>