<? get_header(); ?>
 
 
<div class="block"> <!-- +sidebar -->
<? if (have_posts()): ?>
<? while (have_posts()): the_post(); ?>
<div class="single-post"> <!-- Пост граніца 10px -->
    <h2 class="entry-title"><? the_title(); ?></h2>
    <div class="entry-meta">
					<!-- <p><time pubdate datetime="2012-08-17T15:59:33+00:00">17 жніўня 2012 15:59</time> | <a href="http://budzma.org/category/budzma" title="Прагледзець усе запісы ў рубрыцы &laquo;Навіны &quot;Будзьма!&quot;&raquo;" rel="category tag">Навіны "Будзьма!"</a>   | <a class="post-edit-link" href="http://budzma.org/wp-admin/post.php?post=60166&amp;action=edit" title="Рэдагаваць запіс">Рэдагаваць</a> </p> -->
                    <p><time pubdate datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('j F Y H:i'); ?></time> | <?php the_category(', '); ?> <?php edit_post_link(NULL, ' | '); ?> </p>
    </div>
    <hr noshade size="1"/>
    
    <div class="soderzhanie">
   <!-- <img src="<? bloginfo('template_url');?>/images/435678.jpg" alt="" width="720" height="540"/> -->
    
    <? the_content(); ?>
        
    </div>
    
    
  <hr noshade size="1"/>
  
  <div class="taxo">
        <strong><? the_category('&nbsp;&nbsp;'); ?></strong>&nbsp;&nbsp; 
      </div>
  <hr noshade size="1"/>
  
  <div class="entry-social">
				<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
				<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter,lj,odnoklassniki,friendfeed"></div>
  </div>
  <hr noshade size="1"/>
  <div class="entry-social">
        <div id="vk_like"></div>
        <script type="text/javascript">
         VK.Widgets.Like('vk_like', {width: 500}, 60166);
        </script>
  </div>
  <hr noshade size="1"/>
  
<div id="vk_comments">
    <script type="text/javascript">
    VK.Widgets.Comments("vk_comments", {limit: 10, width: "496", attach: false, autoPublish: 0}, 60166);
    </script>
</div>
<!-- Комментаріі -->

<div class="respond">


<? comments_template(); ?>
<!--
    <h4>Ваш каментар</h4>
    <div class="cancel-comment-reply">
    	<small><a rel="nofollow" id="cancel-comment-reply-link" href="/budzma/vera-bulanda-vystupila-u-v-ikany.html#respond" style="display:none;">Націсніце, каб адмяніць адказ.</a></small>
    </div>
    
    
    <form action="http://budzma.org/wp-comments-post.php" method="post" id="commentform">
    
    	<p class="auth">Вы ўвайшлі як <a href="http://budzma.org/wp-admin/profile.php">admin</a>. <a href="http://budzma.org/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Fbudzma.org%2Fbudzma%2Fvera-bulanda-vystupila-u-v-ikany.html&amp;_wpnonce=eebcc13f88">Выйсці</a></p>
    
    <p><textarea name="comment" id="comment" cols="70" rows="10" tabindex="4"></textarea></p>
    
    <p><input name="submit" type="submit" id="submit" tabindex="5" value="пакінуць каментар" />
    <input type='hidden' name='comment_post_ID' value='60166' id='comment_post_ID' />
    <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
    </p>
    <input type="hidden" id="_wp_unfiltered_html_comment" name="_wp_unfiltered_html_comment" value="3ca5623359" /><p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="1fdd2ecb01" /></p>
    </form>
-->
</div>
  
<!-- конец Комментаріі -->  
</div>

<? endwhile; ?>
<? endif; ?>



<? get_sidebar(); ?> <!-- подключаем sidebar -->
 
 
</div>  <!-- end block -->



<? get_footer(); ?>