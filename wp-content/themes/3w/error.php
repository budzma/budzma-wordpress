<?php
/**
 * @package WordPress
 * @subpackage Budzma_2011
 */
?>

<section class="post">

	<article id="post-0">

		<div class="entry-wrapper">

			<header class="entry-header">
				<hgroup>
					<h2 class="entry-title">Нічога не знойдзена</h2>
				</hgroup>
			</header>

			<div class="entry-content">
				<p>Па вашым запыце нічога не знойдзена. Паспрабуйце ўдакладніць запыт:</p>
				<p><?php get_search_form(); ?></p>
			</div>

		</div>

	</article>
</section>