<?php
/**
 * @package WordPress
 * @subpackage Budzma_2011
 */
get_header(); ?>

<div id="content" role="main">

	<div class="posts-wrapper">

		<section class="single">
			<section class="post">
				<article>
					<div class="entry-wrapper">
						<header class="entry-header">
							<hgroup>
								<h2 class="entry-title"><?php the_title(); ?></h2>
							</hgroup>
							<div class="entry-meta"><!----></div>
						</header>

						<div class="entry-content">
							<div class="details">
								
							</div>
						</div>
					</div>
				</article>
			</section>
		</section>

	</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>