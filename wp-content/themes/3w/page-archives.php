<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
get_header(); ?>

<div id="content" role="main">

	<div class="posts-wrapper">

		<div style="float: left; width: 100%;">
			<?php get_search_form(); ?>
		</div>

		<div style="float: left; margin: 10px; width: 30%;">
			<div class="page-title"><h3>Па месяцах:</h3></div>
			<ul>
				<?php wp_get_archives('type=monthly'); ?>
			</ul>
		</div>

		<div style="float: left; margin: 10px; width: 30%;">
			<div class="page-title"><h3>Па тэмах:</h3></div>
			<ul>
				<?php wp_list_categories('title_li='); ?>
			</ul>
		</div>

	</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>