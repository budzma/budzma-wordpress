<?php
/**
 * @package WordPress
 * @subpackage Budzma_2011
 */
// Кастамныя палі
$custom_fields = get_post_custom();

// Крыніца
$source = '';
if ( ! empty($custom_fields['post_source'][0])) {
	if ( ! empty($custom_fields['post_url'][0]))
		$source = ' | <a href="'.$custom_fields['post_url'][0].'">'.$custom_fields['post_source'][0].'</a>';
	else
		$source = ' | '.$custom_fields['post_source'][0];
}
?>

	<article id="post-<?php the_ID(); ?>">

		<div class="entry-wrapper">

			<header class="entry-header">
				<hgroup>
					<h2 class="entry-title"><?php the_title(); ?></h2>
				</hgroup>
				<div class="entry-meta">
					<p><time pubdate datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('j F Y H:i'); ?></time> | <?php the_category(', '); ?> <?php echo $source; ?> <?php edit_post_link(NULL, ' | '); ?> </p>
				</div>
			</header>

			<div class="entry-content">
				<?php if (has_post_thumbnail()) { ?>
				<?php //the_post_thumbnail('large'); ?>
				<?php } ?>

				<div class="details">
					<?php the_content(); ?>
				</div>
			</div>

      <div class="taxo">
        <?php  
          foreach (get_the_category() as $catObj) {
            $catLink = esc_url(get_category_link( $catObj->term_id ));
            echo "<a href=\"{$catLink}\" alt=\"{$catObj->cat_name}\"><strong>{$catObj->cat_name}</strong></a>&nbsp;&nbsp;";
          }
        ?> 
      </div>

			<div class="entry-social">
				<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
				<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter,lj,odnoklassniki,friendfeed"></div>
      </div>
      
      <div class="entry-social">
        <div id="vk_like"></div>
        <script type="text/javascript">
         VK.Widgets.Like('vk_like', {width: 500}, <?php echo the_ID(); ?>);
        </script>
      </div>

			<div class="comments">
				<?php comments_template('', TRUE); ?>
			</div>

		</div>

	</article>