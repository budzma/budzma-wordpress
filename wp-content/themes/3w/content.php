<?php
/**
 * @package WordPress
 * @subpackage Budzma_2011
 */

// Кастамныя палі
$custom_fields = get_post_custom();

// Крыніца
$source = '';
if ( ! empty($custom_fields['post_source'][0])) {
	if ( ! empty($custom_fields['post_url'][0]))
		$source = ' | <a href="'.$custom_fields['post_url'][0].'">'.$custom_fields['post_source'][0].'</a>';
	else
		$source = ' | '.$custom_fields['post_source'][0];
}
?>

<article id="post-<?php the_ID(); ?>" class="post">

	<div class="entry-wrapper">

		<header class="entry-header">
			<hgroup>
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			</hgroup>
			<div class="entry-meta">
				<p><time pubdate datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('j F Y H:i'); ?></time> | <?php the_category(', '); ?> <?php edit_post_link(NULL, ' | '); ?> </p>
			</div>
		</header>

		<div class="entry-content">
			<?php if (has_post_thumbnail()) { ?>
			<?php the_post_thumbnail('thumbnail'); ?>
			<?php } elseif ( ! empty($custom_fields['thumbnail'][0])) { ?>
			<img src="<?php echo $custom_fields['thumbnail'][0]; ?>" alt="" />
			<?php } ?>

			<?php
			$link = get_permalink();
			$content = get_the_content('');
			$content = preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/', "\\2", $content);
			$content = preg_replace('/<p[^>]+>(.*?)<\/p>/', "\\3", $content);
			$content = preg_replace('/<img[^>]+>/i', '', $content);
			echo '<p>'.$content.' <a class="more" href="'.$link.'">Далей »</a></p>';
			?>
		</div>

	</div>

</article>