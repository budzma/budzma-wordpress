<?php
/**
 * @package WordPress
 * @subpackage Budzma_2011
 */
?>

		</div>

	</div>


	<footer>

		<div class="wrapper">

			<div id="copyright">
				<div class="alignleft">
					<div class="counters"><?php get_template_part('counters'); ?></div>
					<div class="links">
						<?php wp_nav_menu(array(
							'container'		=> FALSE,
							'menu_class'	=> '',
							'theme_location'=> 'secondary',
							'before'		=> '<span></span>',
							'link_before'	=> '',
							'link_after'	=> '')
						); ?>
						<p>&copy; BUDZMA.ORG, 2008-2011. Дызайн: Zuker (2009), Adliga (2010)</p>
					</div>
				</div>
				<div class="alignright">
					<form action="<?php bloginfo('url'); ?>/" method="get">
						<?php
						$select = wp_dropdown_categories(array(
							'orderby'	=> 'name',
							'order'		=> 'ASC',
							'hide_empty'=> 1,
							'selected'	=> 0,
							'class'		=> 'switch',
							'id'		=> '',
							'depth'		=> 0,
							'echo'		=> 0,
							'exclude'	=> '1, 2, 4, 14, 7, 282, 158, 151, 152, 162, 22',
							'hierarchical'	=> 1,
							'show_option_all'	=> 'хуткі пераход да тэмаў і разделаў &nbsp;&nbsp;',
							));
						$select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
						echo $select;
						?>
					</form>
				</div>
			</div>

		</div>

	</footer>

</div>

<?php
wp_enqueue_script('jquery_simplyscroll', get_stylesheet_directory_uri().'/js/jquery.simplyscroll.min.js', FALSE, '1.0.4');
wp_enqueue_script('custom_common', get_stylesheet_directory_uri().'/js/common.js', FALSE, '1.0');
wp_print_scripts(array('jquery', 'jquery_simplyscroll', 'custom_common'));
?>

<?php wp_footer(); ?>

</body>
</html>
<?php if (current_user_can('manage_options')) { ?>
<!-- <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. -->
<?php } ?>