jQuery(document).ready(function($){
	// Dropdown menu
	$('menu ul').droppy({speed: 0});
	// Scrolled projects
	//$('#projects ul').simplyScroll({
	//	autoMode: 'loop',
	//	speed: 2
	//});
});

// Droppy 0.1.2
// (c) 2008 Jason Frame (jason@onehackoranother.com)
jQuery.fn.droppy = function(options) {
	var options = jQuery.extend({speed: 250}, options || {});

	this.each(function() {
		var root = this, zIndex = 1000;

		function getSubnav(ele) {
			if (ele.nodeName.toLowerCase() == 'li') {
				var subnav = jQuery('> ul', ele);
				return subnav.length ? subnav[0] : null;
			} else {
				return ele;
			}
		}
		function getActuator(ele) {
			if (ele.nodeName.toLowerCase() == 'ul') {
				return jQuery(ele).parents('li')[0];
			} else {
				return ele;
			}
		}
		function hide() {
			var subnav = getSubnav(this);
			if (!subnav) return;
			jQuery.data(subnav, 'cancelHide', false);
			setTimeout(function() {
				if (!jQuery.data(subnav, 'cancelHide')) {
					jQuery(subnav).slideUp(options.speed);
				}
			}, 0);
		}
		function show() {
			var subnav = getSubnav(this);
			if (!subnav) return;
			jQuery.data(subnav, 'cancelHide', true);
			jQuery(subnav).css({zIndex: zIndex++}).slideDown(options.speed);
			if (this.nodeName.toLowerCase() == 'ul') {
				var li = getActuator(this);
				jQuery(li).addClass('hover');
				jQuery('> a', li).addClass('hover');
			}
		}
		jQuery('ul, li', this).hover(show, hide);
		jQuery('li', this).hover(
			function() { jQuery(this).addClass('hover'); jQuery('> a', this).addClass('hover'); },
			function() { jQuery(this).removeClass('hover'); jQuery('> a', this).removeClass('hover'); }
		);
	});
};

// Default comment reply
addComment={moveForm:function(d,f,i,c){var m=this,a,h=m.I(d),b=m.I(i),l=m.I("cancel-comment-reply-link"),j=m.I("comment_parent"),k=m.I("comment_post_ID");if(!h||!b||!l||!j){return}m.respondId=i;c=c||false;if(!m.I("wp-temp-form-div")){a=document.createElement("div");a.id="wp-temp-form-div";a.style.display="none";b.parentNode.insertBefore(a,b)}h.parentNode.insertBefore(b,h.nextSibling);if(k&&c){k.value=c}j.value=f;l.style.display="";l.onclick=function(){var n=addComment,e=n.I("wp-temp-form-div"),o=n.I(n.respondId);if(!e||!o){return}n.I("comment_parent").value="0";e.parentNode.insertBefore(o,e);e.parentNode.removeChild(e);this.style.display="none";this.onclick=null;return false};try{m.I("comment").focus()}catch(g){}return false},I:function(a){return document.getElementById(a)}};