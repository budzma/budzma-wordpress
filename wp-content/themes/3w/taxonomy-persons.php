<?php get_header( 'persons'); ?>

<?php get_template_part('content', 'persons-header'); ?>

<?php
  $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
  query_posts( array('post_type' => 'asoba', 'persons' => $current_term->slug, 'posts_per_page' => 14, 'paged'=>( get_query_var('paged') ? get_query_var('paged') : 1 )));

?>

<?php get_template_part('content', 'persons-cat'); ?>

<?php get_template_part('content', 'persons-footer'); ?>

<?php get_footer( 'persons'); ?>