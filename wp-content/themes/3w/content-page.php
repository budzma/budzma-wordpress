<?php
/**
 * @package WordPress
 * @subpackage Budzma_2011
 */
?>

<article id="post-<?php the_ID(); ?>">

	<div class="entry-wrapper">

		<header class="entry-header">
			<hgroup>
				<h2 class="entry-title"><?php the_title(); ?></h2>
			</hgroup>
			<div class="entry-meta">
				<p><time pubdate datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('j F Y H:i'); ?></time> | <?php the_category(', '); ?> <?php edit_post_link(NULL, ' | '); ?> </p>
			</div>
		</header>

		<div class="entry-content">
			<div class="details">
				<?php the_content(); ?>
			</div>
		</div>

		<div class="entry-social">
			<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
			<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter,lj,odnoklassniki,friendfeed"></div> 
		</div>

		<div class="comments">
			<?php comments_template('', TRUE); ?>
		</div>

	</div>

</article>