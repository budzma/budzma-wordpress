<? get_header(); ?>
 
 
<div class="block"> <!-- +sidebar -->

<div class="single-post"> <!-- Пост граніца 10px -->

<?php if (have_posts()) : ?>

			

 
 <? while (have_posts()): the_post(); ?>
  <div class="excerp-post" > <!-- start post -->
    <hr noshade size="1"/>
    <h2 class="entry-title-excerpt"><a href="<? the_permalink(); ?>" rel="bookmark"> <? the_title(); ?></a></h2>
    <div class="entry-meta">
				<!--	<p><time pubdate datetime="2012-08-17T15:59:33+00:00">17 жніўня 2012 15:59</time> | <a href="http://budzma.org/category/budzma" title="Прагледзець усе запісы ў рубрыцы &laquo;Навіны &quot;Будзьма!&quot;&raquo;" rel="category tag">Навіны "Будзьма!"</a>   | <a class="post-edit-link" href="http://budzma.org/wp-admin/post.php?post=60166&amp;action=edit" title="Рэдагаваць запіс">Рэдагаваць</a> </p> -->
            <p><time pubdate datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('j F Y H:i'); ?></time> | <?php the_category(', '); ?> <?php edit_post_link(NULL, ' | '); ?> </p>
    </div>
    
    
    <div class="soderzhanie2">
    
   <a href="<? the_permalink(); ?>" > <? the_post_thumbnail(); ?> </a>
    
        <div class="soderzhanie2-text">
        
     <? the_excerpt(); ?>   <!-- <? the_content(); ?> --> <!-- <a href="<? the_permalink(); ?>">Далей »</a> -->
        
        </div>
    </div>
  </div>    <!-- .excerp-post -->   <!-- start post -->
  <? endwhile; ?>
  
  <hr noshade size="1"/> 
  
    <div class="navigator-3w" align="center"> <!-- навигатор -->
  
     <?php
				if (function_exists('wp_pagenavi')) {
					wp_pagenavi();
				} else { ?>
					<div class="alignleft"><?php next_post_link('&larr; папярэднія') ?></div>
					<div class="alignright"><?php previous_post_link('наступныя &rarr;') ?></div>
				<?php } ?>
    </div>
  
  <? else :   ?>
  
  <h2 class="entry-title-excerpt">Па вашым запыце нічога не знойдзена.</h2>
  
  <div class="soderzhanie2">  
        <div class="soderzhanie2-text">
        
        Паспрабуйце змяніць тэкст запыту.
        
        </div>
    </div>
  <hr noshade size="1"/>   
    
                
 <? endif; ?>
 
 
 
</div>






<? get_sidebar(); ?> <!-- подключаем sidebar -->
 
 
</div>  <!-- end block -->



<? get_footer(); ?>