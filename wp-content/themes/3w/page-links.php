<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
get_header(); ?>

<div id="content" role="main">

	<div class="posts-wrapper">

		<section class="single">
			<section class="post">
				<article>
					<div class="entry-wrapper">
						<header class="entry-header">
							<hgroup>
								<h2 class="entry-title"><?php the_title(); ?></h2>
							</hgroup>
							<div class="entry-meta"><!----></div>
						</header>

						<div class="entry-content">
							<div class="details">
								<ul>
									<?php wp_list_bookmarks(array(
										'exclude_category' => '2',
										'between' => ' - ',
										'show_description' => 1,
										'show_images' => 0,
										'title_before' => '<h2>',
										'title_after' => '</h2>',
										'category_before' => '<li id="%id" class="%class">',
										'category_after' => '</li>',
									)); ?>
								</ul>
							</div>
						</div>
					</div>
				</article>
			</section>
		</section>

	</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>