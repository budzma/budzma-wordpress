<?php
  /**
   * Нижняя часть области контента для всех страниц проекта "Асобы".
   * Content Footer Block   
   * 
   * Для CSS:
   * #footer
   * #footerText         
   */     
?>

<div id="footer">
  <a href="/" title="Вярнуцца на budzma.org"><img src="<?php echo get_template_directory_uri(); ?>/persons/img/footer_logo.gif" alt="budzma.org" /></a>
  <div id="footerText">
    &nbsp;
  </div> 
</div>

<?php /* Div#content. Открыт в content-persons-header.php */ ?>
</div>