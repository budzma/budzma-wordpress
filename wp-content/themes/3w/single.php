<? get_header(); ?>
 
 
<div class="block"> <!-- +sidebar -->
<? if (have_posts()): ?>
<? while (have_posts()): the_post(); ?>
<div class="single-post"> <!-- Пост граніца 10px -->

    <h2 class="padzahalovak"><?php echo (get_post_meta($post->ID, 'padzahalovak', true)); ?></h2>
    <h2 class="entry-title"><? the_title(); ?></h2>
    <div class="entry-meta">
					<!-- <p><time pubdate datetime="2012-08-17T15:59:33+00:00">17 жніўня 2012 15:59</time> | <a href="http://budzma.org/category/budzma" title="Прагледзець усе запісы ў рубрыцы &laquo;Навіны &quot;Будзьма!&quot;&raquo;" rel="category tag">Навіны "Будзьма!"</a>   | <a class="post-edit-link" href="http://budzma.org/wp-admin/post.php?post=60166&amp;action=edit" title="Рэдагаваць запіс">Рэдагаваць</a> </p> -->
                    <p><time pubdate datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('j F Y H:i'); ?></time> | <?php the_category(', '); ?> <?php edit_post_link(NULL, ' | '); ?> </p>
    </div>
    <hr noshade size="1"/>
    
    <div class="soderzhanie">
   <!-- <img src="<? bloginfo('template_url');?>/images/435678.jpg" alt="" width="720" height="540"/> -->
    
    <? the_content(); ?>
        
    </div>
   <!-- 5 похожих материалов (5 последних из 2-ой и последующих категорий $category_ids[1] ) -->    
 <!--  
   <div class="artikyls">      
    <h3>Падобныя артыкулы:</h3>
    <?php
    $categories = get_the_category($post->ID);
    if ($categories) {
     $category_ids = array();
     foreach($categories as $individual_category) $category_ids[1] = $individual_category->term_id;
     $args=array(
     'category__in' => $category_ids,
     'post__not_in' => array($post->ID),
     'showposts'=>5,
     'caller_get_posts'=>1);
     $my_query = new wp_query($args);
     if( $my_query->have_posts() ) {
     echo '<ul>';
     while ($my_query->have_posts()) {
     $my_query->the_post();
    ?>
    <li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
    <?php
    }
    echo '</ul>';
    }
    wp_reset_query();
    }
    ?>
   </div>  
-->  
   
   <!-- 5 последних материалов из 2-ой категории  -->
   <div class="artikyls">

<h3>Апошняе ў рубрыцы:</h3>

<?php

$backup = $post;

$categories = get_the_category($post->ID);

$category_ids = array();

if ($categories) {

$tagcount = count($categories);

for ($i = 1; $i < 2; $i++) {

$category_ids[$i] = $categories[$i]->term_id;

}

$args=array(

'category__in' => $category_ids,

'post__not_in' => array($post->ID),

'showposts'=>5,

'caller_get_posts'=>1

);

$my_query = new WP_Query($args);

if( $my_query->have_posts() ) {

while ($my_query->have_posts()) : $my_query->the_post(); ?>

<li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

<?php endwhile;

} else { ?>

<?php
    $categories = get_the_category($post->ID);
    if ($categories) {
     $category_ids = array();
     foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
     $args=array(
     'category__in' => $category_ids,
     'post__not_in' => array($post->ID),
     'showposts'=>5,
     'caller_get_posts'=>1);
     $my_query = new wp_query($args);
     if( $my_query->have_posts() ) {
     echo '<ul>';
     while ($my_query->have_posts()) {
     $my_query->the_post();
    ?>
    <li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
    <?php
    }
    echo '</ul>';
    }
    wp_reset_query();
    }
    ?>

<?php }

}

$post = $backup;

wp_reset_query();

?>

    </div>
   
   
   
   
   
    
  <hr noshade size="1"/>
  <!-- Category & Tags -->
  <div class="taxo">
        <strong><? the_category('&nbsp;&nbsp;'); ?></strong>&nbsp;&nbsp; <strong><?php the_tags(); ?></strong>&nbsp;&nbsp; 
      </div>
  <hr noshade size="1"/>  
  
  
  <div class="entry-social">
				<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
				<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter,lj,odnoklassniki,friendfeed"></div>
  </div>
 <!-- <hr noshade size="1"/> -->
  
  <div class="entry-social">
        <div id="vk_like"></div>
        <script type="text/javascript">
        VK.Widgets.Like("vk_like", {type: "button"});
        </script>
        
        <div id="googleplus">
        <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
<g:plusone size="medium"></g:plusone> 
        </div>
        
        <div id="facebook_like">
        
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=279413112162332";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
        </div>
  </div>
  <!-- <hr noshade size="1"/> -->
  
  <!-- Комментаріі соц. сети 

  <dl class="tabs-s">  
  
    <dt class="selected">У Кантакце</dt>
    <dd class="selected">
    <div class="tab-content" >
            
        <div id="vk_comments"></div>
        <script type="text/javascript">
        VK.Widgets.Comments("vk_comments", {limit: 10, width: "490", attach: "*"});
        </script>
            
    </div>
    </dd>     
    
    
    <dt>Facebook</dt>
    <dd>
    <div class="tab-content" >            
            <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-num-posts="10" data-width="490"></div>           

    </div>
    </dd> 
  
</dl>

<script type="text/javascript">
     jQuery(function(){
         jQuery('dl.tabs-s dt').click(function(){
             jQuery(this)
                .siblings().removeClass('selected').end()
                .next('dd').andSelf().addClass('selected');
        });
    });
</script> 

-->

<div class="entry-social">
<? comments_template(); ?>
</div>
  
<!-- конец Комментаріі -->  
</div>

<? endwhile; ?>
<? endif; ?>



<? get_sidebar(); ?> <!-- подключаем sidebar -->
 
 
</div>  <!-- end block -->



<? get_footer(); ?>