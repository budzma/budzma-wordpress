<?php get_header(); ?>

<div id="allcontent-posts">

			<!--- Галоўнае ---> <?php include (TEMPLATEPATH . '/main-slider.php'); ?>
			<!--- Актуаліі ---> <?php include (TEMPLATEPATH . '/aktualii.php'); ?>
			<!---- Важнае ----> <?php include (TEMPLATEPATH . '/featured-posts.php'); ?>

<div class="clearfix">  </div>
			</div>

<!------------------------------------- Калумістыка, навіны культуры, афіша ------------------------------------------------->

<div id="allcontent-posts-2">

<!--- Калумністыка --->

<div id="kal-and-baner">

			<div id="allkal-posts-slider">
			<?php include (TEMPLATEPATH . '/kalumnistyka.php'); ?>
			 </div> 


			<div class="header-baner">
			 <?php ds_show_banners(2); ?>
      
						</div>
						</div>


			<!--- Афіша --->
 <div id="event-ant-player-sidebar">
<div id="event-sidebar">
	<ul class="event-sidebar-content">
<?php dynamic_sidebar( 'Sidebar-afisha' ); ?>
</ul>
<a href="http://budzma.by/events/upcoming/">Усе падзеі</a>
  </div> 


			<!--- Аўдыё ---> <div class="radio-player">
<h2 class="main-rubryka-v"> Аўдыё </h2>
<div class="radio-player-audio">
<iframe frameborder="0" marginheight="0" marginwidth="0" style="width: 100%; height: 70px; border: 0px;" src="<?php bloginfo('template_directory'); ?>/player/player1.html"></iframe>
</div></div>
</div> 


<div class="hr-line-full">
		
</div>

			<!--- Культурны блок ---> <?php include (TEMPLATEPATH . '/culture-block.php'); ?>
			<!--- Фотарэпартажы ---> <?php include (TEMPLATEPATH . '/photo-rep.php'); ?>
			<!--- Відэа ---> <?php include (TEMPLATEPATH . '/videa.php'); ?>



</div>
</div>	

			<!---- Журналісцкія праекты ----> <?php include (TEMPLATEPATH . '/other-pro.php'); ?>


<div id="allcontent-posts-3">

			<!----- Культура | Краіна | Навіны "Будзьма" ---------> <?php include (TEMPLATEPATH . '/kultura.php'); ?>

<div id="aktualii-kp">



<?php $my_query = new WP_Query('cat=462,901&showposts=1'); while ($my_query->have_posts()) : $my_query->the_post(); ?>

<div class="komiks-img">
<a href="<?php the_permalink() ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(800,800), array('class' => 'alignleft othernews-post-image')); } ?></a>
</div>

<div class="meta-kalumnistyka">
<?php exclude_post_categories('144, 707, 282, 163, 712, 710',', '); ?>
</div>

<h2 class="komiks-post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title=""><?php the_title(); ?></a> </h2>
<div class="featured-post-except">
<?php limits2(370, ""); ?></div>
</div>

<?php endwhile; ?>
</div>



<div class="allcontent-posts-pro">
<div class="allcontent-posts-pro-content">
<h2 class="main-rubryka">
НАШЫ ПРАЕКТЫ
	</h2>

<!---- Праекты ---->
<?php include (TEMPLATEPATH . '/pro.php'); ?>
</div>

</div>


<?php get_footer(); ?>