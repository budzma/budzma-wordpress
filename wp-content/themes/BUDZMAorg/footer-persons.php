<?php
  /**
   * Шаблон для Футера для всех страниц проекта "Асобы".   
   */     
?>

</div> <!-- end of div#karkas -->
<div>
  <!--Google Analytics-->
  <script type="text/javascript">
  var gaJsHost = (("https:" == document.location.protocol) ?
  "https://ssl." : "http://www.");
  document.write(unescape("%3Cscript src='" + gaJsHost +
  "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
  </script>
  <script type="text/javascript">
  var pageTracker = _gat._getTracker("UA-7161554-1");
  pageTracker._addOrganic("tut.by", "query");
  pageTracker._addOrganic("search.tut.by", "query");
  pageTracker._addOrganic("all.by", "query");
  pageTracker._addOrganic("rambler.ru", "words");
  pageTracker._addOrganic("nova.rambler.ru", "query");
  pageTracker._addOrganic("go.mail.ru", "q");
  pageTracker._initData();
  pageTracker._trackPageview();
  </script>
  <!--End Of Google Analytics-->
</div>
<?php wp_footer(); ?>
</body></html>
<?php /* инфа для разраба */ ?>
<?php if (current_user_can('manage_options')) { ?>
<!-- <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. -->
<?php } ?>