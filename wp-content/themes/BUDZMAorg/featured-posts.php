<?php
global $options;
foreach ($options as $value) {
	if (get_settings( $value['id'] ) === FALSE) {
		$$value['id'] = $value['std'];
	}
	else {
		$$value['id'] = get_settings( $value['id'] );
	}
}
?>

<div id="featured-posts">
<h2 class="main-rubryka">
Важнае
	</h2>

 <div id="slider">
            <div class="simple"">
                
<ul id="featured-posts-list">
      
		<?php $my_query = new WP_Query('cat=707&showposts=4'); while ($my_query->have_posts()) : $my_query->the_post(); ?>
        	<li>
            		<div class="featured-post-image">
<?php if (imagesrc()) { ?>
					<a href="<?php the_permalink() ?>" rel="bookmark" alt="<?php the_title(); ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(190,190), array('class' => 'aligncenter post_thumbnail')); } ?></a>
				<?php } ?>
				
			</div>

			<div class="featured-post-text">

<div class="meta-kalumnistyka">
<?php exclude_post_categories('144, 707, 282, 163, 712, 710, 758',', '); ?>
</div>

				<h2 class="featured-post-title"><a href="<?php the_permalink() ?>" rel="bookmark" alt="<?php the_title(); ?>"><?php the_title(); ?></a> </h2>    



<div class="meta-comments-number"> <a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a></div>

<div class="meta_categories0">
<a href="<?php the_permalink() ?>"><img src="<?php
  $categories = get_the_category();
  foreach($categories as $key => $category) {
    $url = get_term_link((int)$category->term_id,'category');
    $categories[$key] =
      "{$category->category_description}";
  }
  echo "\n" . implode("\n",$categories) . "\n";
?>" /></a>
</div>
        			
</div>

		</li>
		<?php endwhile; ?>

	</ul>

            </div>

            <div class="light">
               <ul id="featured-posts-list">
      
		<?php $my_query = new WP_Query('cat=707&showposts=4&offset=4'); while ($my_query->have_posts()) : $my_query->the_post(); ?>
        	<li>
            		<div class="featured-post-image">

<?php if (imagesrc()) { ?>
					<a href="<?php the_permalink() ?>" rel="bookmark" alt="<?php the_title(); ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(190,190), array('class' => 'aligncenter post_thumbnail')); } ?></a>
				<?php } ?>
				
			</div>

			<div class="featured-post-text">


<div class="meta-kalumnistyka">
<?php exclude_post_categories('144, 707, 282, 163, 712, 710, 758',', '); ?>
</div>


				<h2 class="featured-post-title"><a href="<?php the_permalink() ?>" rel="bookmark" alt="<?php the_title(); ?>"><?php the_title(); ?></a> </h2>    


<div class="meta-comments-number">
 <a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a>
</div>

<div class="meta_categories0">
<a href="<?php the_permalink() ?>"><img src="<?php
  $categories = get_the_category();
  foreach($categories as $key => $category) {
    $url = get_term_link((int)$category->term_id,'category');
    $categories[$key] =
      "{$category->category_description}";
  }
  echo "\n" . implode("\n",$categories) . "\n";
?>" /></a>
</div>

        
			</div>

		</li>
		<?php endwhile; ?>

	</ul>
            </div>
            <div class="easy">
                <ul id="featured-posts-list">
      
		<?php $my_query = new WP_Query('cat=707&showposts=4&offset=8'); while ($my_query->have_posts()) : $my_query->the_post(); ?>
        	<li>
            		<div class="featured-post-image">

<?php if (imagesrc()) { ?>
					<a href="<?php the_permalink() ?>" rel="bookmark" alt="<?php the_title(); ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(190,190), array('class' => 'aligncenter post_thumbnail')); } ?></a>
				<?php } ?>
				
			</div>


			<div class="featured-post-text">


<div class="meta-kalumnistyka">
<?php exclude_post_categories('144, 707, 282, 163, 712, 710, 758',', '); ?>
</div>


				<h2 class="featured-post-title"><a href="<?php the_permalink() ?>" rel="bookmark" alt="<?php the_title(); ?>"><?php the_title(); ?></a> </h2>    


<div class="meta-comments-number"> <a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a></div>


<div class="meta_categories0">
<a href="<?php the_permalink() ?>"><img src="<?php
  $categories = get_the_category();
  foreach($categories as $key => $category) {
    $url = get_term_link((int)$category->term_id,'category');
    $categories[$key] =
      "{$category->category_description}";
  }
  echo "\n" . implode("\n",$categories) . "\n";
?>" /></a>
</div>

       
			</div>

		</li>
		<?php endwhile; ?>

	</ul>
            </div>
        </div>


 <script type="text/javascript">

		jQuery(document).ready(function(){
            jQuery('#slider').slider({
                autoplay: false,
		showProgress: true
            });
        });
        </script>


</div>