<?php

/* ------- Adding a custom menu ------- */
add_theme_support('menus');

add_action( 'init', 'register_my_menus' );

function register_my_menus() {
	register_nav_menus(
		array(
			'menu-1' => __( 'Menu 1' ),
			'menu-projects' => __( 'Menu Projects' ),
		)
	);
}



if (function_exists('add_theme_support')) {
add_theme_support('post-thumbnails');
}



/* ------- Схаваць панэль Wordpress для ўсіх акрамя адмінаў------- */


 if (!current_user_can('administrator')):
  show_admin_bar(false);
endif;







/**
 * Раздел "Асобы".
 */


function bpers_set_tpl_vars() {
  bpers_set_tpl_vars_post();
  bpers_set_tpl_vars_prev_post();
  bpers_set_tpl_vars_next_post();
  global $post;
  //wp_pear_debug::dump( $post);
}

function bpers_set_tpl_vars_post() {
  global $post;
  //wp_pear_debug::dump( $post);
  if ($post->post_type == 'asoba') {
    $first = array();
    $first['type'] = FALSE; // говорит о том, что профайл сломан
    $second = array();
    $second['type'] = FALSE; // говорит о том, что профайл сломан

    $custom = get_post_custom();
    //wp_pear_debug::dump( $custom);

    $permalink = get_permalink( $post->ID);

    $title = $post->post_title;

    $title_few_lines = '';
    if (isset($custom['title_few_lines'][0])) {
      $title_few_lines = $custom['title_few_lines'][0];
    }
    //wp_pear_debug::dump( $title_few_lines);

    $slogan = '';
    if (isset($custom['slogan'][0])) {
      $slogan = $custom['slogan'][0];
    }

    $slogan_few_lines = '';
    if (isset($custom['slogan_few_lines'][0])) {
      $slogan_few_lines = $custom['slogan_few_lines'][0];
    }

    $text_first_column = '';
    if (isset($custom['column1'][0])) {
      $text_first_column = $custom['column1'][0];
    }

    $text_second_column = '';
    if (isset($custom['column2'][0])) {
      $text_second_column = $custom['column2'][0];
    }

    $photo_first = FALSE;
    if (isset($custom['photo'][0])) {
      $tmp = wp_get_attachment_image_src( $custom['photo'][0], 'full');
      if ($tmp) {
        $photo_first = array(
          'url' => $tmp[0],
          'width' => $tmp[1],
          'height' => $tmp[2],
        );

        if ($photo_first['width'] < $photo_first['height']) {
          $first['type'] = 'vertical';
        }
        else {
          $first['type'] = 'horizontal';
        }
      }
    }
    //wp_pear_debug::dump( $photo_first);

    $photo_second = '';
    if (isset($custom['photo2'][0])) {
      $tmp = wp_get_attachment_image_src( $custom['photo2'][0], 'full');
      if ($tmp) {
        $photo_second = array(
          'url' => $tmp[0],
          'width' => $tmp[1],
          'height' => $tmp[2],
        );

        if ($photo_second['width'] < $photo_second['height']) {
          $second['type'] = 'vertical';
        }
        else {
          $second['type'] = 'horizontal';
        }
      }
    }
    //wp_pear_debug::dump( $photo_second);

    $thumb_first = FALSE;
    if (isset($custom['_thumbnail_id'][0])) {
      $tmp = wp_get_attachment_image_src( $custom['_thumbnail_id'][0], 'full');
      if ($tmp) {
        $thumb_first = array(
          'url' => $tmp[0],
          'width' => $tmp[1],
          'height' => $tmp[2],
        );
      }
    }
    //wp_pear_debug::dump( $thumb_first);

    $thumb_second = FALSE;
    if (isset($custom['thumb2'][0])) {
      $tmp = wp_get_attachment_image_src( $custom['thumb2'][0], 'full');
      if ($tmp) {
        $thumb_second = array(
          'url' => $tmp[0],
          'width' => $tmp[1],
          'height' => $tmp[2],
        );
      }
    }
    //wp_pear_debug::dump( $thumb_second);

    //wp_pear_debug::dump( $first);
    //wp_pear_debug::dump( $second);

    $terms = array();
    $term = FALSE;
    $tmp = get_the_terms( $post->ID, 'persons');
    if (is_array($tmp) && !empty($tmp)) {
      $terms = $tmp;
      $term = array_shift( $tmp);
    }
    //wp_pear_debug::dump( $term);

    // можно будет сменить формат

    $post->bpers = array();

    $post->bpers['permalink'] = $permalink;
    $post->bpers['term'] = $term;

    $post->bpers['title'] = $title;
    $post->bpers['title_few_lines'] = $title_few_lines;

    $post->bpers['slogan'] = $slogan;
    $post->bpers['slogan_few_lines'] = $slogan_few_lines;

    $post->bpers['thumb'] = $thumb_first;

    $post->bpers['text_first_column'] = $text_first_column;
    $post->bpers['text_second_column'] = $text_second_column;

    $post->bpers['first'] = $first;
    $post->bpers['first']['photo'] = $photo_first;
    $post->bpers['first']['thumb'] = $thumb_first;

    $post->bpers['second'] = $second;
    $post->bpers['second']['photo'] = $photo_second;
    $post->bpers['second']['thumb'] = $thumb_second;

    //wp_pear_debug::dump( $post->bpers);
  }
}

function bpers_set_tpl_vars_prev_post() {
  global $post;
  if (isset($post->bpers)) {
    $prev_post = array_shift( get_adjacent_post_plus(
      array(
        'order_by' => 'post_date',
        'loop' => true,
        'end_post' => false,
        'in_same_cat' => 'persons',
        'in_same_tax' => 'persons',
        'echo' => false,
      ),
      true
    ));

    if (isset($prev_post->ID)) {
      $custom = get_post_custom( $prev_post->ID);
      //wp_pear_debug::dump( $custom);

      $permalink = get_permalink( $prev_post->ID);

      $title = $prev_post->post_title;

      $title_few_lines = '';
      if (isset($custom['title_few_lines'][0])) {
        $title_few_lines = $custom['title_few_lines'][0];
      }

      $slogan = '';
      if (isset($custom['slogan'][0])) {
        $slogan = $custom['slogan'][0];
      }

      $slogan_few_lines = '';
      if (isset($custom['slogan_few_lines'][0])) {
        $slogan_few_lines = $custom['slogan_few_lines'][0];
      }

      $thumb_first = FALSE;
      if (isset($custom['_thumbnail_id'][0])) {
        $tmp = wp_get_attachment_image_src( $custom['_thumbnail_id'][0], 'full');
        if ($tmp) {
          $thumb_first = array(
            'url' => $tmp[0],
            'width' => $tmp[1],
            'height' => $tmp[2],
          );
        }
      }
      //wp_pear_debug::dump( $thumb_first);

      $thumb_second = FALSE;
      if (isset($custom['thumb2'][0])) {
        $tmp = wp_get_attachment_image_src( $custom['thumb2'][0], 'full');
        if ($tmp) {
          $thumb_second = array(
            'url' => $tmp[0],
            'width' => $tmp[1],
            'height' => $tmp[2],
          );
        }
      }
      //wp_pear_debug::dump( $thumb_second);

      $terms = array();
      $term = FALSE;
      $tmp = get_the_terms( $prev_post->ID, 'persons');
      if (is_array($tmp) && !empty($tmp)) {
        $terms = $tmp;
        $term = array_shift( $tmp);
      }
      //wp_pear_debug::dump( $term);

      // можно будет сменить формат

      $post->bpers['prev_post'] = array();

      $post->bpers['prev_post']['permalink'] = $permalink;
      $post->bpers['prev_post']['term'] = $term;

      $post->bpers['prev_post']['title'] = $title;
      $post->bpers['prev_post']['title_few_lines'] = $title_few_lines;

      $post->bpers['prev_post']['slogan'] = $slogan;
      $post->bpers['prev_post']['slogan_few_lines'] = $slogan_few_lines;

      $post->bpers['prev_post']['thumb'] = $thumb_first;

      //wp_pear_debug::dump( $post->bpers['prev_post']);
    }
  }
}

function bpers_set_tpl_vars_next_post() {
  global $post;
  if (isset($post->bpers)) {
    $next_post = array_shift( get_adjacent_post_plus(
      array(
        'order_by' => 'post_date',
        'loop' => true,
        'end_post' => false,
        'in_same_cat' => 'persons',
        'in_same_tax' => 'persons',
        'echo' => false,
      ),
      false
    ));

    if (isset($next_post->ID)) {
      $custom = get_post_custom( $next_post->ID);
      //wp_pear_debug::dump( $custom);

      $permalink = get_permalink( $next_post->ID);

      $title = $next_post->post_title;

      $title_few_lines = '';
      if (isset($custom['title_few_lines'][0])) {
        $title_few_lines = $custom['title_few_lines'][0];
      }

      $slogan = '';
      if (isset($custom['slogan'][0])) {
        $slogan = $custom['slogan'][0];
      }

      $slogan_few_lines = '';
      if (isset($custom['slogan_few_lines'][0])) {
        $slogan_few_lines = $custom['slogan_few_lines'][0];
      }

      $thumb_first = FALSE;
      if (isset($custom['_thumbnail_id'][0])) {
        $tmp = wp_get_attachment_image_src( $custom['_thumbnail_id'][0], 'full');
        if ($tmp) {
          $thumb_first = array(
            'url' => $tmp[0],
            'width' => $tmp[1],
            'height' => $tmp[2],
          );
        }
      }
      //wp_pear_debug::dump( $thumb_first);

      $thumb_second = FALSE;
      if (isset($custom['thumb2'][0])) {
        $tmp = wp_get_attachment_image_src( $custom['thumb2'][0], 'full');
        if ($tmp) {
          $thumb_second = array(
            'url' => $tmp[0],
            'width' => $tmp[1],
            'height' => $tmp[2],
          );
        }
      }
      //wp_pear_debug::dump( $thumb_second);

      $terms = array();
      $term = FALSE;
      $tmp = get_the_terms( $next_post->ID, 'persons');
      if (is_array($tmp) && !empty($tmp)) {
        $terms = $tmp;
        $term = array_shift( $tmp);
      }
      //wp_pear_debug::dump( $term);

      // можно будет сменить формат

      $post->bpers['next_post'] = array();

      $post->bpers['next_post']['permalink'] = $permalink;
      $post->bpers['next_post']['term'] = $term;

      $post->bpers['next_post']['title'] = $title;
      $post->bpers['next_post']['title_few_lines'] = $title_few_lines;

      $post->bpers['next_post']['slogan'] = $slogan;
      $post->bpers['next_post']['slogan_few_lines'] = $slogan_few_lines;

      $post->bpers['next_post']['thumb'] = $thumb_first;

      //wp_pear_debug::dump( $post->bpers['next_post']);
    }
  }
}


function bpers_set_first_current() {
  global $post;
  if ($post->post_type == 'asoba' && isset($post->bpers['first']) && $post->bpers['first']['type']) {
    $post->bpers['current'] = $post->bpers['first'];
    $post->bpers['current']['css_id'] = 'first';
  }
}
function bpers_set_second_current() {
  global $post;
  if ($post->post_type == 'asoba' && isset($post->bpers['second']) && $post->bpers['second']['type']) {
    $post->bpers['current'] = $post->bpers['second'];
    $post->bpers['current']['css_id'] = 'second';
  }
}





/* ------- Не адлюстроўваць некаторыя катэгорыі ------- */

function exclude_post_categories($excl='', $spacer=' '){
   $categories = get_the_category($post->ID);
      if(!empty($categories)){
      	$exclude=$excl;
      	$exclude = explode(",", $exclude);
		$thecount = count(get_the_category()) - count($exclude);
      	foreach ($categories as $cat) {
      		$html = ' ';
      		if(!in_array($cat->cat_ID, $exclude)) {
				$html .= '<a href="' . get_category_link($cat->cat_ID) . '" ';
				$html .= 'title="' . $cat->cat_name . '">' . $cat->cat_name . '</a>';
				if($thecount>1){
					$html .= $spacer;
				}
			$thecount--;
      		echo $html;
      		}
	      }
      }
}






/* Функция для вывода последних комментариев в WordPress. Параметры: 
$limit - сколко комментов выводить. По дефолту - 10 
$ex - обрезка текста комментария до n символов. По дефолту - 45 
$cat - Включить(5,12,35) или исключить(-5,-12,-35) категории, указываются id категорий через запятую. По дефолту - пусто - из всех категорий. 
$echo - выводить на экран (1) или возвращать (0). По дефолту - 1 
$gravatar - показывать иконку gravatar, указывается размер иконки, например, 20 - выведет иконку шириной и высотой в 20px 
===================================================================================== */  
function kama_recent_comments($limit=10, $ex=45, $cat=0, $echo=1, $gravatar=''){  
    global $wpdb;  
    if($cat){  
        $IN = (strpos($cat,'-')===false)?"IN ($cat)":"NOT IN (".str_replace('-','',$cat).")";  
        $join = "LEFT JOIN $wpdb->term_relationships rel ON (p.ID = rel.object_id)  
        LEFT JOIN $wpdb->term_taxonomy tax ON (rel.term_taxonomy_id = tax.term_taxonomy_id)";  
        $and = "AND tax.taxonomy = 'category'  
        AND tax.term_id $IN";  
    }  
    $sql = "SELECT comment_ID, comment_post_ID, comment_content, post_title, guid, comment_author, comment_author_email  
    FROM $wpdb->comments com  
        LEFT JOIN $wpdb->posts p ON (com.comment_post_ID = p.ID) {$join}  
    WHERE comment_approved = '1'  
        AND comment_type = '' {$and}  
    ORDER BY comment_date DESC  
    LIMIT $limit";   
  
    $results = $wpdb->get_results($sql);  
  
    $out = '';  
    foreach ($results as $comment){  
        if($gravatar)  
            $grava = '<img src="http://www.gravatar.com/avatar/'. md5($comment->comment_author_email) .'?s=$gravatar&default=" alt="" width="'. $gravatar .'" height="'. $gravatar.'" />';  
        $comtext = strip_tags($comment->comment_content);  
        $leight = (int) iconv_strlen( $comtext, 'utf-8' );  
        if($leight > $ex) $comtext =  iconv_substr($comtext,0,$ex, 'UTF-8').' …';  
        $out .= "\n<li>$grava<b>".strip_tags($comment->comment_author). ": </b><a href='". get_comment_link($comment->comment_ID) ."' title='{$comment->post_title}'>{$comtext}</a></li>";  
    }  
  
    if ($echo) echo $out;  
    else return $out;  
}  







/* ------- Register sidebar ------- */
if ( function_exists('register_sidebars') )
    
	register_sidebar(array('name'=>'Sidebar')); //сайдбар
	register_sidebar(array('name'=>'Sidebar-afisha')); //сайдбар (для афішы)
	register_sidebar(array('name'=>'Sidebar-video')); //сайдбар (для відэа)
	register_sidebar(array('name'=>'Sidebar-video2')); //сайдбар (для відэа 2)
	register_sidebar(array('name'=>'Sidebar-video3')); //сайдбар (для відэа 3)
	register_sidebar(array('name'=>'Sidebar-video4')); //сайдбар (для відэа 4)
	register_sidebar(array('name'=>'Sidebar-video5')); //сайдбар (для відэа 5)
	register_sidebar(array('name'=>'Sidebar-video6')); //сайдбар (для відэа 6)
	register_sidebar(array('name'=>'Sidebar-kamientary')); //сайдбар (для апошніх каментараў)
	register_sidebar(array('name'=>'Sidebar-pops')); //сайдбар (для папулярнае)


function limits($max_char, $more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
	$content = strip_tags($content, '');

   if (strlen($_GET['p']) > 0) {
      echo $content;
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        echo $content;

        echo "...";
        echo "<div class=";
		echo "'continue-reading'>";
		echo "<a href='";
        the_permalink();
        echo "'>".$more_link_text."</a></div>";
   }
   else {
      echo $content;
   }
}
function limits2($max_char, $more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
	$content = strip_tags($content, '');

   if (strlen($_GET['p']) > 0) {
      echo $content;
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        echo $content;

        echo "...";
   }
   else {
      echo $content;
   }
}
function zt_get_thumbnail($postid=0, $size='thumbnail', $attributes='') {
	if ($postid<1) $postid = get_the_ID();
	if ($images = get_children(array(
		'post_parent' => $postid,
		'post_type' => 'attachment',
		'numberposts' => 1,
		'post_mime_type' => 'image', )))
		foreach($images as $image) {
			$thumbnail=wp_get_attachment_image_src($image->ID, $size);
			?>
<img src="<?php echo $thumbnail[0]; ?>" <?php echo $attributes; ?> alt="<?php the_title(); ?>" />
<?php
		}
	else {
?>
<img src="<?php bloginfo('template_directory'); ?>/images/noimage.jpg" alt="<?php the_title(); ?>" />
<?php
	}
	
}


function get_first_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)) {
    echo "";
  }
  else {
?>
	<img src="<?php echo $first_img; ?>" alt="<?php the_title(); ?>" />
<?php
  }
}


function imagesrc() {
global $post, $posts;
$first_img = '';
ob_start();
ob_end_clean();
$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
$first_img = $matches [1] [0];
if (!($first_img))
{
	$attachments = get_children(array('post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'image', 'orderby' => 'menu_order'));
if (is_array($attachments))
	{
	$count = count($attachments);
	$first_attachment = array_shift($attachments);
	$imgsrc = wp_get_attachment_image_src($first_attachment->ID, 'large');
	$first_img = $imgsrc[0];
	}
}
return $first_img;
}
?>
<?php

$themename = "BUDZMA";
$shortname = "yam";
$options = array (

array(
"name" => "Мінімальныя наладкі тэмы",
"type" => "title"),

array(
"type" => "open"),

array(
"name" => "Спасылка на лагатып",
"desc" => "Пазначце тут спасылку.",
"id" => $shortname."_logourl",
"std" => "http://new.budzma.org/wp-content/uploads/2013/11/budzma_logo-m1.png",
"type" => "text"),

array(
"name" => "Favicon URL",
"desc" => "Пазначце тут спасылку.",
"id" => $shortname."_favicon",
"std" => "http://new.budzma.org//wp-content/themes/BUDZMAorg/images/favicon.png",
"type" => "text"),








array(
"type" => "close")

);

/* ------- Add a Theme Options Page ------- */
function mytheme_add_admin() {

    global $themename, $shortname, $options;

    if ( $_GET['page'] == basename(__FILE__) ) {

        if ( 'save' == $_REQUEST['action'] ) {

                foreach ($options as $value) {
                    update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }

                foreach ($options as $value) {
                    if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }

                header("Location: themes.php?page=functions.php&saved=true");
                die;

        } else if( 'reset' == $_REQUEST['action'] ) {

            foreach ($options as $value) {
                delete_option( $value['id'] ); }

            header("Location: themes.php?page=functions.php&reset=true");
            die;

        }
    }

    add_theme_page($themename." Options", "".$themename." Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');

}

function mytheme_admin() {

    global $themename, $shortname, $options;

    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';

?>
<div class="wrap" style="margin:0 auto; padding:20px 0px 0px;">

<form method="post">

<?php foreach ($options as $value) {
switch ( $value['type'] ) {

case "open":
?>
<div style="width:808px; background:#eee; border:1px solid #ddd; padding:20px; overflow:hidden; display: block; margin: 0px 0px 30px;">

<?php break;

case "close":
?>

</div>

<?php break;

case "misc":
?>
<div style="width:808px; background:#fffde2; border:1px solid #ddd; padding:20px; overflow:hidden; display: block; margin: 0px 0px 30px;">
	<?php echo $value['name']; ?>
</div>
<?php break;

case "title":
?>

<div style="width:810px; height:22px; background:#555; padding:9px 20px; overflow:hidden; margin:0px; font-family:Verdana, sans-serif; font-size:18px; font-weight:normal; color:#EEE;">
	<?php echo $value['name']; ?>
</div>

<?php break;

case 'text':
?>

<div style="width:808px; padding:0px 0px 10px; margin:0px 0px 10px; border-bottom:1px solid #ddd; overflow:hidden;">
	<span style="font-family:Arial, sans-serif; font-size:16px; font-weight:bold; color:#444; display:block; padding:5px 0px;">
		<?php echo $value['name']; ?>
	</span>
	<?php if ($value['image'] != "") {?>
		<div style="width:808px; padding:10px 0px; overflow:hidden;">
			<img style="padding:5px; background:#FFF; border:1px solid #ddd;" src="<?php bloginfo('template_url');?>/images/<?php echo $value['image'];?>" alt="image" />
		</div>
	<?php } ?>
	<input style="width:200px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id'] )); } else { echo stripslashes($value['std']); } ?>" />
	<br/>
	<span style="font-family:Arial, sans-serif; font-size:11px; font-weight:bold; color:#444; display:block; padding:5px 0px;">
		<?php echo $value['desc']; ?>
	</span>
</div>

<?php
break;

case 'textarea':
?>

<div style="width:808px; padding:0px 0px 10px; margin:0px 0px 10px; border-bottom:1px solid #ddd; overflow:hidden;">
	<span style="font-family:Arial, sans-serif; font-size:16px; font-weight:bold; color:#444; display:block; padding:5px 0px;">
		<?php echo $value['name']; ?>
	</span>
	<?php if ($value['image'] != "") {?>
		<div style="width:808px; padding:10px 0px; overflow:hidden;">
			<img style="padding:5px; background:#FFF; border:1px solid #ddd;" src="<?php bloginfo('template_url');?>/images/<?php echo $value['image'];?>" alt="image" />
		</div>
	<?php } ?>
	<textarea name="<?php echo $value['id']; ?>" style="width:400px; height:200px;" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id'] )); } else { echo stripslashes($value['std']); } ?></textarea>
	<br/>
	<span style="font-family:Arial, sans-serif; font-size:11px; font-weight:bold; color:#444; display:block; padding:5px 0px;">
		<?php echo $value['desc']; ?>
	</span>
</div>

<?php
break;

case 'select':
?>

<div style="width:808px; padding:0px 0px 10px; margin:0px 0px 10px; border-bottom:1px solid #ddd; overflow:hidden;">
	<span style="font-family:Arial, sans-serif; font-size:16px; font-weight:bold; color:#444; display:block; padding:5px 0px;">
		<?php echo $value['name']; ?>
	</span>
	<?php if ($value['image'] != "") {?>
		<div style="width:808px; padding:10px 0px; overflow:hidden;">
			<img style="padding:5px; background:#FFF; border:1px solid #ddd;" src="<?php bloginfo('template_url');?>/images/<?php echo $value['image'];?>" alt="image" />
		</div>
	<?php } ?>
		<select style="width:240px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
	<?php foreach ($value['options'] as $option_value => $option_text) { 
	   $checked = ' ';
        if (get_settings($value['id']) == $option_text) {
            $selected = ' selected="selected" ';
        }
        else if (get_settings($value['id']) === FALSE && $value['std'] == $option_text){
            $selected = ' selected="selected" ';
        }
        else {
            $selected = ' ';
        }
    ?>
    <option <?php echo "value=".$option_text." ".$selected; ?> ><?php echo $option_text; ?></option>
	<?php } ?>
    </select>
	<br/>
	<span style="font-family:Arial, sans-serif; font-size:11px; font-weight:bold; color:#444; display:block; padding:5px 0px;">
		<?php echo $value['desc']; ?>
	</span>
</div>

<?php
break;

case "checkbox":
?>

<div style="width:808px; padding:0px 0px 10px; margin:0px 0px 10px; border-bottom:1px solid #ddd; overflow:hidden;">
	<span style="font-family:Arial, sans-serif; font-size:16px; font-weight:bold; color:#444; display:block; padding:5px 0px;">
		<?php echo $value['name']; ?>
	</span>
	<?php if ($value['image'] != "") {?>
		<div style="width:808px; padding:10px 0px; overflow:hidden;">
			<img style="padding:5px; background:#FFF; border:1px solid #ddd;" src="<?php bloginfo('template_url');?>/images/<?php echo $value['image'];?>" alt="image" />
		</div>
	<?php } ?>
	<?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
	<input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
	<br/>
	<span style="font-family:Arial, sans-serif; font-size:11px; font-weight:bold; color:#444; display:block; padding:5px 0px;">
		<?php echo $value['desc']; ?>
	</span>
</div>


<?php
break;

case "submit":
?>

<p class="submit">
<input name="save" type="submit" value="Save changes" />
<input type="hidden" name="action" value="save" />
</p>

<?php break;
}
}
?>

<p class="submit">
<input name="save" type="submit" value="Save changes" />
<input type="hidden" name="action" value="save" />
</p>
</form>
<form method="post">
<p class="submit">
<input name="reset" type="submit" value="Reset" />
<input type="hidden" name="action" value="reset" />
</p>
</form>



























































<?php
}
function mytheme_wp_head() { ?>
<?php }
add_action('wp_head', 'mytheme_wp_head');
add_action('admin_menu', 'mytheme_add_admin'); ?>