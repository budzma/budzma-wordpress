<?php
/*
Template Name Posts: Ствараем культуру
*/
?>


<?php get_header(); ?>


<div id="allcontent-posts-single">
	<div id="content">
		<div id="posts">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="full-post" id="post-<?php the_ID(); ?>"> 
					<h2 class="full-post-title"><?php the_title(); ?></h2>
					<div class="full-post-content"><?php the_content(); ?></div>
					<div class="full-post-pages"><?php wp_link_pages(); ?></div>
					<div class="meta-full-post">
					</div>
					<div id="culture-managment">
					<div id="allkal-posts-kultura">
						<div class="section">
							<ul class="tabs">
								<li class="current"><h2 class="main-rubryka-tabs">Робім культуру</h2></li>
								<li><h2 class="main-rubryka-tabs2">Абмяркоўваем культуру</h2></li>
								<li><h2 class="main-rubryka-tabs2">Бібліятэка карысных ведаў</h2></li>
							</ul>
							<div class="box visible">
								<div class="kal-posts-kultura">
									<div class="kalumnistyka-main">
									<?php $my_query = new WP_Query( 'p=103991' ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
									
									<div class="kalumnistyka-image">
										<a href="<?php the_permalink() ?>"><?php if (imagesrc()) { ?>
											<img src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php $thumb_id = get_post_thumbnail_id(); $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true); echo $thumb_url[0]; ?>&amp;w=380&amp;h=286" /><?php } ?>
										</a>
									</div>
									<div class="meta-kalumnistyka">
										<?php exclude_post_categories('144, 707, 282, 163, 712, 710, 758, 1181',', '); ?>
									</div>
									<h2 class="kalumnistyka-post-title">
										<a href="<?php the_permalink() ?>" rel="bookmark" title=""><?php the_title(); ?></a>
									</h2>
									<div class="featured-post-except">
										<?php limits2(270, ""); ?>
									</div>
									<div class="meta-comments-number">
										<a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a>
									</div>
									<div class="meta_categories0">
										<a href="<?php the_permalink() ?>">
											<?php
											  $categories = get_the_category();
											  foreach($categories as $key => $category) {
												$url = get_term_link((int)$category->term_id,'category');
												  $cat_pic = $category->category_description;
												  if ($cat_pic) {
													echo "<img src='" . $cat_pic . "' />";
												  }
											  }
											?>
										</a>
									</div>
									<div class="clearfix"></div>
									<h2 class="link-to-all-possts"><a href="http://budzma.by/kultura-managment/robim-kulturu">Усе навіны рубрыкі</a></h2>
								</div>
								<?php endwhile; 
								wp_reset_query(); ?>
								
						<!-- Калумністыка (усе пасты) -->
									<div class="kalumnistyka-all">
										<?php 
										$exclude_ids = array( 103991 );
										$args = array(
											'cat'                 => 1181,
											'post__not_in'        => $exclude_ids,
											'posts_per_page'      => 4
										);
										$my_query = new WP_Query( $args ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
											<div class="kalumnistyka-all-image">
												<a href="<?php the_permalink() ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(190,190), array('class' => 'alignleft othernews-post-image')); } ?></a>
											</div>
											<div class="kalumnistyka-post-text">
												<div class="meta-kalumnistyka"><?php exclude_post_categories('144, 707, 282, 163, 712, 710, 758, 1181',', '); ?></div>
												<h2 class="kalumnistyka-all-title">
													<a href="<?php the_permalink() ?>" rel="bookmark" title=""><?php the_title(); ?></a>
												</h2>
												<div class="meta-comments-number">
													<a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a>
												</div>
												<div class="meta_categories0">
													<a href="<?php the_permalink() ?>">
														<?php
														  $categories = get_the_category();
														  foreach($categories as $key => $category) {
															$url = get_term_link((int)$category->term_id,'category');
															  $cat_pic = $category->category_description;
															  if ($cat_pic) {
																echo "<img src='" . $cat_pic . "' />";
															  }
														  }
														?>
													</a>
												</div>
											</div>
										<?php endwhile;
										wp_reset_query(); ?>
									</div>
								</div>
							</div>
							<div class="box">
								<div class="kal-posts-kultura">
									<div class="kalumnistyka-main">
									<?php $my_query = new WP_Query( 'p=104000' ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
									
									<div class="kalumnistyka-image">
										<a href="<?php the_permalink() ?>"><?php if (imagesrc()) { ?>
											<img src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php $thumb_id = get_post_thumbnail_id(); $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true); echo $thumb_url[0]; ?>&amp;w=380&amp;h=286" /><?php } ?>
										</a>
									</div>
									<div class="meta-kalumnistyka">
										<?php exclude_post_categories('144, 707, 282, 163, 712, 710, 758, 1180',', '); ?>
									</div>
									<h2 class="kalumnistyka-post-title">
										<a href="<?php the_permalink() ?>" rel="bookmark" title=""><?php the_title(); ?></a>
									</h2>
									<div class="featured-post-except">
										<?php limits2(270, ""); ?>
									</div>
									<div class="meta-comments-number">
										<a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a>
									</div>
									<div class="meta_categories0">
										<a href="<?php the_permalink() ?>">
											<?php
											  $cat_count = 0;
											  $categories = get_the_category();
											  foreach($categories as $key => $category) {
												$url = get_term_link((int)$category->term_id,'category');
												  $cat_pic = $category->category_description;
												  if ($cat_pic) {
													echo "<img src='" . $cat_pic . "' />";
												  }
											  }
											?>
										</a>
									</div>
									<div class="clearfix"></div>
									<h2 class="link-to-all-possts"><a href="http://budzma.by/kultura-managment/abmerkavanne-kultury">Усе навіны рубрыкі</a></h2>
								</div>
								<?php endwhile; 
								wp_reset_query(); ?>
						<!-- Калумністыка (усе пасты) -->
									<div class="kalumnistyka-all">
										<?php 
										$exclude_ids = array( 104000 );
										$args = array(
											'cat'                 => 1180,
											'post__not_in'        => $exclude_ids,
											'posts_per_page'      => 4
										);
										$my_query = new WP_Query( $args ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
											<div class="kalumnistyka-all-image">
												<a href="<?php the_permalink() ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(190,190), array('class' => 'alignleft othernews-post-image')); } ?></a>
											</div>
											<div class="kalumnistyka-post-text">
												<div class="meta-kalumnistyka"><?php exclude_post_categories('144, 707, 282, 163, 712, 710, 758, 1180',', '); ?></div>
												<h2 class="kalumnistyka-all-title">
													<a href="<?php the_permalink() ?>" rel="bookmark" title=""><?php the_title(); ?></a>
												</h2>
												<div class="meta-comments-number">
													<a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a>
												</div>
												<div class="meta_categories0">
													<a href="<?php the_permalink() ?>">
														<?php
														  $cat_count = 0;
														  $categories = get_the_category();
														  foreach($categories as $key => $category) {
															$url = get_term_link((int)$category->term_id,'category');
															  $cat_pic = $category->category_description;
															  if ($cat_pic) {
																echo "<img src='" . $cat_pic . "' />";
															  }
														  }
														?>
													</a>
												</div>
											</div>
										<?php endwhile; ?>
									</div>
								</div>
							</div>
							<div class="box">
								<div class="kal-posts-kultura">
									<div class="kalumnistyka-main">
									<?php $my_query = new WP_Query( 'p=104004' ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
									
									<div class="kalumnistyka-image">
										<a href="<?php the_permalink() ?>"><?php if (imagesrc()) { ?>
											<img src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php $thumb_id = get_post_thumbnail_id(); $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true); echo $thumb_url[0]; ?>&amp;w=380&amp;h=286" /><?php } ?>
										</a>
									</div>
									<div class="meta-kalumnistyka">
										<?php exclude_post_categories('144, 707, 282, 163, 712, 710, 758, 1182',', '); ?>
									</div>
									<h2 class="kalumnistyka-post-title">
										<a href="<?php the_permalink() ?>" rel="bookmark" title=""><?php the_title(); ?></a>
									</h2>
									<div class="featured-post-except">
										<?php limits2(270, ""); ?>
									</div>
									<div class="meta-comments-number">
										<a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a>
									</div>
									<div class="meta_categories0">
										<a href="<?php the_permalink() ?>">
											<?php
											  $cat_count = 0;
											  $categories = get_the_category();
											  foreach($categories as $key => $category) {
												$url = get_term_link((int)$category->term_id,'category');
												  $cat_pic = $category->category_description;
												  if ($cat_pic) {
													echo "<img src='" . $cat_pic . "' />";
												  }
												
											  }
											?>
										</a>
									</div>
									<div class="clearfix"></div>
									<h2 class="link-to-all-possts"><a href="http://budzma.by/kultura-managment/bibliateka">Усе навіны рубрыкі</a></h2>
								</div>
								<?php endwhile; ?>
						<!-- Калумністыка (усе пасты) -->
									<div class="kalumnistyka-all">
										<?php 
										$exclude_ids = array( 104004 );
										$args = array(
											'cat'                 => 1182,
											'post__not_in'        => $exclude_ids,
											'posts_per_page'      => 4
										);
										$my_query = new WP_Query( $args ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
											<div class="kalumnistyka-all-image">
												<a href="<?php the_permalink() ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(190,190), array('class' => 'alignleft othernews-post-image')); } ?></a>
											</div>
											<div class="kalumnistyka-post-text">
												<div class="meta-kalumnistyka"><?php exclude_post_categories('144, 707, 282, 163, 712, 710, 758, 1182',', '); ?></div>
												<h2 class="kalumnistyka-all-title">
													<a href="<?php the_permalink() ?>" rel="bookmark" title=""><?php the_title(); ?></a>
												</h2>
												<div class="meta-comments-number">
													<a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a>
												</div>
												<div class="meta_categories0">
													<a href="<?php the_permalink() ?>">
														<?php
														  $cat_count = 0;
														  $categories = get_the_category();
														  foreach($categories as $key => $category) {
															$url = get_term_link((int)$category->term_id,'category');
															  $cat_pic = $category->category_description;
															  if ($cat_pic) {
																echo "<img src='" . $cat_pic . "' />";
															  }
															
														  }
														?>
													</a>
												</div>
											</div>
										<?php endwhile; ?>
									</div>
								</div>
							</div>
							
						</div><!-- .section -->
					
					</div>
					</div>
					<!-- Put this div tag to the place, where the Like block will be -->
					<div id="vk_like"></div>
					<script type="text/javascript">
					VK.Widgets.Like("vk_like", {type: "button", height: 18});
					</script>

					<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>

					<a href="https://twitter.com/share" class="twitter-share-button" data-via="budzma">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

					</br>
					<div class="clearfix"></div>
					<?php comments_template(); ?>
				</div><!-- full-post -->
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<?php get_sidebar(); ?>

	</div>
</div>

<?php get_footer(); ?>