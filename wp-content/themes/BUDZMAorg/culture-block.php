<h2 class="main-rubryka main-rubryka-mleft">Ствараем культуру</h2>
<div id="culture-slider">
	<div class="slider-content"> 
		<div class="slideshoy1">
			<div id="lofslidecontent55" class="lof-slidecontent">
				<div class="preload">
					<div></div>
				</div>
				<!-- MAIN CONTENT -->
				<div class="lof-main-wapper">
					<?php $my_query = new WP_Query( 'p=104015' ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
						<div class="lof-main-item">
							<div class="mainthumb-slider">
								<a href="<?php the_permalink() ?>" rel="bookmark" alt="<?php the_title_attribute(); ?>">	<?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(420,420), array('class' => 'aligncenter post_thumbnail')); } ?></a>
							</div>
							<div class="lof-main-item-desc">
								<p>
									<a href="<?php the_permalink() ?>">
										<?php limits2(540, ""); ?>
									</a>
									<div class="meta-comments-number-slider">
										<a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a>
									</div>
									<div class="meta_categories0-slider">
										<a href="<?php the_permalink() ?>">
											<img src="<?php
											  $categories = get_the_category();
											  foreach($categories as $key => $category) {
												$url = get_term_link((int)$category->term_id,'category');
												$categories[$key] =
												  "{$category->category_description}";
											  }
											  echo "\n" . implode("\n",$categories) . "\n";
											?>" />
										</a>
									</div>
								</p>
							</div>
						</div>
					<?php endwhile; 
					wp_reset_query(); ?>
					<?php query_posts('cat=1208&showposts=2'); ?>
						<?php if (have_posts()) : while (have_posts()) : the_post();?>
							<div class="lof-main-item">
								<div class="mainthumb-slider">
									<a href="<?php the_permalink() ?>" rel="bookmark" alt="<?php the_title_attribute(); ?>">	<?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(420,420), array('class' => 'aligncenter post_thumbnail')); } ?></a>
								</div>
								<div class="lof-main-item-desc">
									<p>
										<a href="<?php the_permalink() ?>">
											<?php limits2(540, ""); ?>
										</a>
										<div class="meta-comments-number-slider">
											<a href="<?php the_permalink() ?>"><?php comments_number('', '1', '%'); ?></a>
										</div>
										<div class="meta_categories0-slider">
											<a href="<?php the_permalink() ?>">
												<img src="<?php
												  $categories = get_the_category();
												  foreach($categories as $key => $category) {
													$url = get_term_link((int)$category->term_id,'category');
													$categories[$key] =
													  "{$category->category_description}";
												  }
												  echo "\n" . implode("\n",$categories) . "\n";
												?>" />
											</a>
										</div>
									</p>
								</div>
							</div>
						<?php endwhile; ?>
						<?php endif;  
						wp_reset_query(); ?>
				</div>
	  <!-- END MAIN CONTENT --> 
		<!-- NAVIGATOR -->
				<div class="lof-navigator-outer">
					<ul class="lof-navigator">
						<?php $my_query = new WP_Query( 'p=104015' ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
							<li>
								<div>
									<h3> <a href="<?php the_permalink() ?>"> <?php the_title(); ?></a> </h3>
								</div>
							</li>
						<?php endwhile; 
						wp_reset_query(); ?>
						<?php query_posts('cat=1208&showposts=2'); ?>
							<?php if (have_posts()) : while (have_posts()) : the_post();?>
								<li>
									<div>
										<h3> <a href="<?php the_permalink() ?>"> <?php the_title(); ?></a> </h3>
									</div>
								</li>
							<?php endwhile; ?>
							<?php endif; ?>
					</ul>
				</div>
			</div>
			
	<script type="text/javascript">
		var _lofmain2 =  $('lofslidecontent55');
		var _lofscmain2 = _lofmain2.getElement('.lof-main-wapper');
		var _lofnavigator2= _lofmain2.getElement('.lof-navigator-outer .lof-navigator');
		var object = new LofFlashContent( _lofscmain2, 
										  _lofnavigator2,
										  _lofmain2.getElement('.lof-navigator-outer'),
										  { fxObject:{ transition:Fx.Transitions.Quad.easeInOut,  duration:800},
											 interval:6000,
											 direction:'opacity' } );
		object.start( true, _lofmain2.getElement('.preload') );
	</script>



	</div> <!-- slideshoy1 -->

	</div>
</div>
<div id="culture-categories">
	<?php $args = array(
			'post__in' => array( 103991, 104000, 104004 ),
			'orderby' => 'ID',
			'order' => 'ASC',
			'posts_per_page' => 3,
			'ignore_sticky_posts' => 1
		);
		$my_query = new WP_Query( $args ); while ($my_query->have_posts()) : $my_query->the_post();
	?>
	<div class="kalumnistyka-all-image">
		<a href="<?php the_permalink() ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(190,190), array('class' => 'alignleft othernews-post-image')); } ?></a>
	</div>
	<div class="kalumnistyka-post-text">
		<h2 class="kalumnistyka-all-title culture-title">
			<a href="<?php the_permalink() ?>" rel="bookmark" title=""><?php the_title(); ?></a>
		</h2>
	</div>
	<?php endwhile;
	wp_reset_query(); ?>
</div>