<?php get_header( 'persons'); ?>

<?php get_template_part('content', 'persons-header'); ?>

<?php query_posts( array('post_type'=>'asoba', 'posts_per_page'=>14, 'paged'=>( get_query_var('paged') ? get_query_var('paged') : 1 ))); ?>

<?php get_template_part('content', 'persons-cat'); ?>

<?php get_template_part('content', 'persons-footer'); ?>

<?php get_footer( 'persons'); ?>