<?php
  /**
   * Шаблон для Хидера для всех страниц проекта "Асобы". 
   * 
   * Для CSS:
   * #karkas           
   */     
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title(); ?></title>
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" />    
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/persons/persons.css" />
<?php wp_head(); ?>
<style type="text/css">
  div.hidden-content {
    display: none;
  }
</style>
<script type="text/javascript">
  jQuery(document).ready(
    function() {
      jQuery("div#content-second").addClass('hidden-content');
      
      jQuery("div#switcher-first").click(function() {
        var first  = jQuery("div#content-first");
        var second = jQuery("div#content-second"); 
        if (first.hasClass('hidden-content')) {
          second.addClass('hidden-content');
          first.removeClass('hidden-content');
        }
        else {
          first.addClass('hidden-content');
          second.removeClass('hidden-content');
        }
      });
      
      jQuery("div#switcher-second").click(function() {
        var first  = jQuery("div#content-first");
        var second = jQuery("div#content-second");
        if (first.hasClass('hidden-content')) {
          second.addClass('hidden-content');
          first.removeClass('hidden-content');
        }
        else {
          first.addClass('hidden-content');
          second.removeClass('hidden-content');
        }
      });
    }
  );	
  
</script>
</head>
<body>  
<div id="karkas"> 