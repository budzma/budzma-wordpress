<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">

	<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> <?php } ?> <?php wp_title(); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats please -->

        
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,600,300,800,700,400italic|PT+Serif:400,400italic" />

	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />



	


<!-- jQuery -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
<!-- Слайдэр галоўных навін -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/javascript/mootoolssvn.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/javascript/lofslidernews.mt11.js"></script>


<!-- jQuery fade-in, fade-out for images -->
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".lof-main-item img, .pro-images, .header-baner, .featured-post-text img, .meta-comments-number, .single-post-image img, .featured-post-image img, #allkal-posts img, #allkal-posts-kultura img, #aktualii-kp img, .ngg-gallery-thumbnail img, .padobnyja-posts-image").fadeTo("slow", 1);
		jQuery(".lof-main-item img, .pro-images, .header-baner, .featured-post-text img, .meta-comments-number, .single-post-image img, .featured-post-image img, #allkal-posts img, #allkal-posts-kultura img, #aktualii-kp img, .ngg-gallery-thumbnail img, .padobnyja-posts-image").hover(function(){
			jQuery(this).fadeTo("slow", 0.6);
		},function(){
	   			jQuery(this).fadeTo("slow", 1);
			});
		});
	</script>


	<?php
	global $options;
	foreach ($options as $value) {
		if (get_settings( $value['id'] ) === FALSE) {
			$$value['id'] = $value['std'];
		}
		else {
			$$value['id'] = get_settings( $value['id'] );
		}
	}
	?>

	<?php wp_get_archives('type=monthly&format=link'); ?>
	<?php //comments_popup_script(); // off by default ?>
	<?php if (function_exists('wp_enqueue_script') && function_exists('is_singular')) : ?>
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<?php endif; ?>

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $yam_favicon; ?>" />


<!-- facebook.com -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- end facebook.com --> 





<!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?105"></script>

<script type="text/javascript">
  VK.init({apiId: 4065884, onlyWidgets: true});
</script>

	<?php wp_head(); ?>

<!-- Кантэнт-слайдар -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/javascript/jquery-slider.min.js"></script>

<!-- Таб-слайдар -->
<script type="text/javascript">
(function(jQuery) {
jQuery(function() {
	jQuery('ul.tabs').each(function(i) {
		var storage = localStorage.getItem('tab'+i);
		if (storage) jQuery(this).find('li').eq(storage).addClass('current').siblings().removeClass('current')
			.parents('div.section').find('div.box').hide().eq(storage).show();
	})
	jQuery('ul.tabs').on('click', 'li:not(.current)', function() {
		jQuery(this).addClass('current').siblings().removeClass('current')
			.parents('div.section').find('div.box').eq(jQuery(this).index()).fadeIn(150).siblings('div.box').hide();
		var ulIndex = jQuery('ul.tabs').index(jQuery(this).parents('ul.tabs'));
		localStorage.removeItem('tab'+ulIndex);
		localStorage.setItem('tab'+ulIndex, jQuery(this).index());
	})

})
})(jQuery)
</script>


</head>
<body>

<div class="top-baner"><center> <?php ds_show_banners(1); ?> </center></div>


<div id="body-list">
	<div id="header">
	
		<div class="header-bottom">
			<div class="logo" style="background-image: url(<?php echo $yam_logourl; ?>);">
				<h1><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
			</div>

<div class="headersearch-form">
		<form action="<?php bloginfo('url'); ?>/" method="get">
			<input type="text" value="Пошук..." name="s" id="ls" class="searchfield" onfocus="if (this.value == 'Пошук...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Пошук...';}" />
		</form>
	</div>


<div class="login_box"> 
  <?php global $user_ID, $user_identity, $user_level; ?>
  <?php if (is_user_logged_in()) { ?>
    <?php echo get_avatar( $user_ID, 30 ); ?> 
    <div class="user_aside">
      <span class="user_name">  <?php echo $user_identity; ?> | </span>      
      <a class="logout_button" href="<?php echo wp_logout_url( $_SERVER['REQUEST_URI'] ); ?>">Выйсці</a>
    </div>
  <?php } else { ?> 
    <form id="login" action="login" method="post">
      <p class="status_login"></p>
     
      <div class="line" style="display: none;">
        <input name="rememberme" type="checkbox" id="my-rememberme" checked="checked" value="forever" />
      </div>
 
      <div class="line cf">

     <a href="http://budzma.by/wp-login.php" target="_blank">   <input class="submit_button" type="submit" value="Увайсці" name="submit"></a>        
      </div>
 
    </form>
<?php } ?>
 
</div>


<div class="social">

	<div class="about-menu"><a href="http://budzma.by/about">ПРА НАС</a> </div>

<div class="about-menu"><a href="http://budzma.by/smi-pra-budzma">СМІ ПРА НАС</a> </div>


<div id="nav-projects">
<li>
	<a href="#" title="">ПРАЕКТЫ</a>
<ul>	
	<?php wp_nav_menu( array( 'theme_location' => 'menu-projects' ) ); ?>
</ul>
</li>
</div>


<div class="about-menu">
<a href="http://budzma.by/partners"> ПАРТНЁРЫ</a>
</div>


<a href="https://www.facebook.com/budzmabelarusami" target="_blank"><img onmouseover="this.src='<?php bloginfo('template_directory'); ?>/images/f.png'" onmouseout="this.src='<?php bloginfo('template_directory'); ?>/images/facebook.png'" src="<?php bloginfo('template_directory'); ?>/images/facebook.png" style="margin-left: 5px;" alt="" width="25" height="25" /></a>

<a href="http://vk.com/budzmabelarusami" target="_blank"><img onmouseover="this.src='<?php bloginfo('template_directory'); ?>/images/vk_.png'" onmouseout="this.src='<?php bloginfo('template_directory'); ?>/images/vkontakte.png'" src="<?php bloginfo('template_directory'); ?>/images/vkontakte.png" style="margin-left: 5px;" alt="" width="25" height="25" /></a>

<a href="https://twitter.com/budzma" target="_blank"><img onmouseover="this.src='<?php bloginfo('template_directory'); ?>/images/t.png'" onmouseout="this.src='<?php bloginfo('template_directory'); ?>/images/twitter.png'" src="<?php bloginfo('template_directory'); ?>/images/twitter.png" style="margin-left: 5px;" alt="" width="25" height="25" /></a>

<a href="http://www.youtube.com/user/TheBudzma/videos" target="_blank"><img onmouseover="this.src='<?php bloginfo('template_directory'); ?>/images/y.png'" onmouseout="this.src='<?php bloginfo('template_directory'); ?>/images/youtube.png'" src="<?php bloginfo('template_directory'); ?>/images/youtube.png" style="margin-left: 5px;" alt="" width="25" height="25" /></a>

				</div>
	
		</div>

<div class="header-top">
			<div class="main-menu-container">
				<?php wp_nav_menu(array('theme_location' => 'menu-1', 'container' => 'div', 'container_class' => 'main-menu')); ?>
				
			</div>
		</div>
	</div>