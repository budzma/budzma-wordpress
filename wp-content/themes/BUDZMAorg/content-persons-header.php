<?php
  /**
   * Верхняя часть области контента для всех страниц проекта "Асобы".
   * Content Header Block   
   * 
   * Для CSS:
   * #head
   * .right
   * #bp-block-navi
   * .active
   * .left
   * .grey
   * #bp-block-about
   * .center
   * #content         
   */     

  global $post;
  
  $cat_list = get_categories( 
    array(
      'taxonomy' => 'persons',
      'hide_empty' => 0,
    )
  );
  
  $active_cat_slug = 'index';
  $logo_link = '/persons';
  
  if (isset($post->bpers)) {
    if ($post->bpers['term']) {
      $active_cat_slug = $post->bpers['term']->slug;
      //echo '<pre>';
      //print_r($post->bpers);
      //echo '</pre>';
      $logo_link = '/'. $post->bpers['term']->taxonomy .'/'. $post->bpers['term']->slug;
    }
  }
  else {
    // 
    //wp_pear_debug::dump( $post);
    $current_term = get_term_by( 'slug', get_query_var( 'term' ), 'persons' );
    if (isset($current_term->slug)) {
      $active_cat_slug = $current_term->slug;
      //echo '<pre>';
      //print_r($current_term);
      //echo '</pre>';
      $logo_link = '/'. $current_term->taxonomy .'/'. $current_term->slug;
    }
  }
?>

<div id="head">
  <div class="right">
    <div class="grey">
      <?php $last_k = count( $cat_list) - 1; ?>
      <div id="bp-block-navi">
        У праекце <a href="/persons">Асобы</a><br />
        <?php foreach ($cat_list as $k => $item) {
          $html = '<a';
          if ($item->id == $active_cat_id) {
            $html.= ' class="active"';
          }
          $html.= ' href="/'. $item->taxonomy .'/'. $item->slug .'">';
          $html.= $item->name .'</a>';
          if ($k != $last_k) {
            $html.= ', ';
          }
          echo $html;
        }
        ?>
      </div>
    </div>
  </div>
  <div class="left">
    <div class="grey">
      <div id="bp-block-about">
        Асобы кампаніі “Будзьма беларусамі!”<br /><br />
        У гэтым раздзеле нашага сайта мы знаёмім вас “у твар” з выдатнымі асобамі кампаніі “Будзьма беларусамі!”.<br /><br />
        ... <a href="/persons-about">падрабязней пра праект</a>
      </div>
    </div>
  </div>
  <div class="center">
    <a href="<?php echo $logo_link; ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/persons/head_logo/head_logo_<?php echo $active_cat_slug; ?>.gif" alt="" />
    </a>
  </div>
</div>

<?php /* Div#content. Закрывается в content-persons-footer.php */ ?>
<div id="content">