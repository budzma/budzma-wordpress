<?php
/*
Template Name Posts: Культурная старонка
*/
?>


<?php get_header(); ?>


<div id="allcontent-posts-single">
	<div id="content">
		<div id="posts">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="full-post" id="post-<?php the_ID(); ?>"> 
					<h2 class="full-post-title"><?php the_title(); ?></h2>
					<div class="full-post-content"><?php the_content(); ?></div>
					<div class="full-post-pages"><?php wp_link_pages(); ?></div>
					<div class="meta-full-post">
					</div>
					<?php
					$post_id = get_the_ID();
					 if (($post_id == 103991) || ($post_id == 104000)) {
						if ($post_id == 103991) {
							$post_cat = 1181;
						} else {
							$post_cat = 1180;
						}
					?>
					<div id="culture-managment">
					<div id="allkal-posts-kultura">
						<div class="section">
							<ul class="tabs">
								<li class="current"><h2 class="main-rubryka-tabs"><?php the_title(); ?></h2></li>
							</ul>
								<div class="box visible">
									<div class="kal-posts-kultura">
									<?php
									$category_link = get_category_link( $post_cat );
									$exclude_ids = array( $post_id );
									$args = array(
										'cat' => $post_cat,
										'orderby' => 'date',
										'order' => 'DESC',
										'posts_per_page' => 8,
										'post__not_in' => $exclude_ids,
										'ignore_sticky_posts' => 1
									);
									$my_query = new WP_Query( $args ); while ($my_query->have_posts()) : $my_query->the_post(); ?>
										<div class="kalumnistyka-all-kult">
											<div class="kalumnistyka-all-image">
												<a href="<?php the_permalink() ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(190,190), array('class' => 'alignleft othernews-post-image')); } ?></a>
											</div>
										<div class="meta-kalumnistyka">
											<?php the_tags( '', ', ', ''); ?></div>
											<h2 class="kalumnistyka-all-title-kult"><a href="<?php the_permalink() ?>" rel="bookmark" title=""><?php the_title(); ?></a></h2>
										</div>
									<?php endwhile; ?>
									<h2 class="link-to-all-possts"><a href="<?php echo $category_link ?>">Усе навіны рубрыкі</a></h2>
								</div>
							</div>
						</div><!-- .section -->
					</div>
					</div>
					<?php } ?>
					<!-- Put this div tag to the place, where the Like block will be -->
					<div id="vk_like"></div>
					<script type="text/javascript">
					VK.Widgets.Like("vk_like", {type: "button", height: 18});
					</script>

					<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>

					<a href="https://twitter.com/share" class="twitter-share-button" data-via="budzma">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

					</br>
					<div class="clearfix"></div>
					<?php comments_template(); ?>
				</div><!-- full-post -->
				<?php endwhile; ?>
			<?php endif; 
			wp_reset_query(); ?>
		</div>
		<?php get_sidebar(); ?>

	</div>
</div>

<?php get_footer(); ?>