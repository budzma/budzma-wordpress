<?php get_header(); ?>

<div id="allcontent-posts-single">


	<div id="content">

		<div id="posts">

			<?php if (have_posts()) : ?>

				<div class="search-results"><h2>Вынікі пошуку па запыце "<?php echo $_GET['s']; ?>":</h2></div>

				<?php while (have_posts()) : the_post(); ?>

				<div class="single-post" id="post-<?php the_ID(); ?>"> 

					
					

<div class="single-post-text">


<div class="archiveposts-image">			
<a href="<?php the_permalink() ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(150,150), array('class' => 'alignleft othernews-post-image')); } ?></a>
</div>
<div class="meta-full-post-archive"><?php exclude_post_categories('144,707,282,163,712,710,758',', '); ?> <span>| <?php the_time('d.m.Y'); ?></span></div>

						<h2 class="kalumnistyka-post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
						


<div class="featured-post-except">
<?php limits2(470, ""); ?>
</div>

						

					</div><!-- single-post-text -->
					<div class="clearfix"></div>

				</div><!-- single-post -->

				<?php endwhile; ?>

				<div class="posts-navigation">
					<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
			<div class="alignleft"><?php next_posts_link('&laquo; Яшчэ') ?></div>
			<div class="alignright"><?php previous_posts_link('Назад &raquo;') ?></div>
			<?php } ?>
					
				</div>


<?php else: ?>

				<div class="search-results"><h2>Па запыце "<?php echo $_GET['s']; ?>" нічога не знойдзена, паспрабуйце яшчэ раз!</h2></div>


			<?php endif; ?>

		</div>

<?php get_sidebar(); ?>

</div></div>

<?php get_footer(); ?>