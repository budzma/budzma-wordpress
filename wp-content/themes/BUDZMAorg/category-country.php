<?php get_header(); ?>

<div id="allcontent-posts-single">


	<div id="content">

		<div id="posts">

			<?php if (have_posts()) : ?>

				<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
				<?php /* If this is a category archive */ if (is_category()) { ?>
					<div class="search-results"><h2><?php single_cat_title(); ?></h2></div>
				<?php /* If this is a tag archive */ } elseif (is_tag()) { ?>
					<div class="search-results"><h2><?php single_tag_title(); ?></h2></div>
				<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
					<div class="search-results"><h2>Archive for <?php the_time('F jS, Y'); ?>:</h2></div>
				<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
					<div class="search-results"><h2>Archive for <?php the_time('F, Y'); ?>:</h2></div>
				<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
					<div class="search-results"><h2>Archive for <?php the_time('Y'); ?>:</h2></div>
				<?php /* If this is an author archive */ } elseif (is_author()) { ?>
					<div class="search-results"><h2>Author Archive:</h2></div>
				<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
					<div class="search-results"><h2>Blog Archives:</h2></div>
				<?php } ?>

				<?php while (have_posts()) : the_post(); ?>

				<div class="single-post" id="post-<?php the_ID(); ?>"> 

					
					

<div class="single-post-text">


<div class="archiveposts-image">			
<a href="<?php the_permalink() ?>"><?php if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { the_post_thumbnail(array(190,190), array('class' => 'alignleft othernews-post-image')); } ?></a>
</div>

<div class="meta-kalumnistyka"><?php the_time('d.m.Y'); ?> | <?php the_tags( '', ', ', ''); ?></div>
						<h2 class="kalumnistyka-post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
						


<div class="featured-post-except">
<?php limits2(470, ""); ?>
</div>

						

					</div><!-- single-post-text -->
					<div class="clearfix"></div>

				</div><!-- single-post -->

				<?php endwhile; ?>

				<div class="posts-navigation">
					<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
			<div class="alignleft"><?php next_posts_link('&laquo; Яшчэ') ?></div>
			<div class="alignright"><?php previous_posts_link('Назад &raquo;') ?></div>
			<?php } ?>
					<?php endif; ?>
				</div>

		</div>

<?php get_sidebar(); ?>

</div></div>

<?php get_footer(); ?>