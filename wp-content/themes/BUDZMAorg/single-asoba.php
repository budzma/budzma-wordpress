<?php
/*
* Проект "Асобы"
* Шаблон профайла асобы.
*/

/*while ( have_posts() ) {
  the_post();
  bpers_set_tpl_vars();
  if (isset($post->bpers) && $post->bpers['first']['type'] && $post->bpers['second']['type']) {
    get_header( 'asoba');
    get_template_part('content', 'asoba-head');
    bpers_set_first_current();
    get_template_part('content', 'asoba');
    bpers_set_second_current();
    get_template_part('content', 'asoba');
    get_footer( 'asoba');
  }
  else {
    die('Error...');
  }
} */

get_header( 'persons');

while ( have_posts() ) {
  the_post();
  bpers_set_tpl_vars();
  if (isset($post->bpers) && $post->bpers['first']['type'] && $post->bpers['second']['type']) {
    
    get_template_part('content', 'persons-header');
    
    bpers_set_first_current();
    get_template_part('content', 'persons-item');
    bpers_set_second_current();
    get_template_part('content', 'persons-item');
    
    get_template_part('content', 'persons-prevnext');
    
    get_template_part('content', 'persons-footer');
  }
  else {
    die('Error...');
  }
}

get_footer( 'persons');
